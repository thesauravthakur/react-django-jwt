import React from "react";
import usertypeReducer from "./store/usertype/reducer"

import { render } from 'react-dom';
import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import { persistStore } from 'redux-persist'
import { persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import storage from 'redux-persist/lib/storage'
import thunk from "redux-thunk";
import authReducer from "./store/reducers/auth";
import settingReducer from "./store/settings/reducer"
import App from './components/App';
const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['auth', 'setting','usertype']
}
const rootReducer = combineReducers({
	usertype:usertypeReducer,
    auth: authReducer,
    setting: settingReducer,
});


const store = createStore(persistReducer(persistConfig, rootReducer), composeEnhances(applyMiddleware(thunk)));
const persistor = persistStore(store);


const app = (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>
);

render(app, document.getElementById("root"));

// render(
//     <App />,
//     document.querySelector('#root')
// );