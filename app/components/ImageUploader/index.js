import React from 'react';
import { 
    FormGroup, 
    Label, 
    Row,
    Button,
    InputGroup,
    InputGroupAddon
} from '..';
import ImageUploading from 'react-images-uploading';
import axiosInstance from '../../axios.js'


export default class Image extends React.Component{
    state={
        images:[],
        maxNumberOfImage:0,
        first:true,
        loading:false,
        imageUpload:false,
        imageUpdate:false,
        imageDelete:false,
    }
    componentDidMount(){
        this.state.images=this.props.images
        this.state.maxNumberOfImage=this.props.maxNumberOfImage
        this.setState({images:this.props.images,maxNumberOfImage:this.props.maxNumberOfImage})
    }

    render(){

        const postData=async (formData=null, index=null)=>{
            formData['name']=formData['file']['name']
            try {
                    const resp = await axiosInstance.post('api/v1/image/',formData);
                    if (resp.data['message']!=null && resp.data['message']!=undefined){
                        console.log('Item deleted')
                    }else{
                    this.state.images[index]['data_url']='http://127.0.0.1:8000'+resp.data['image']
                    this.state.images[index]['image_url']=resp.data['image']
                    this.state.images[index]['image_id']=resp.data['id']
                    this.setState({images:this.state.images})
                    }
                } catch (err) {
                    // Handle Error Here
                    console.error(err);
                }
        }

        const onChange = async (imageList, addUpdateIndex) => {
            if(this.state.imageUpload){
            if(addUpdateIndex!=undefined && addUpdateIndex!=null){
            addUpdateIndex.map(item=>{
                // let formData = new FormData();
                // formData.append('image', imageList[item]['file'], imageList[item]['file']['name']);
                imageList[item]['type']='create'
                postData(imageList[item], item)
            })
            }
            this.setState({images:imageList})
            this.props.parentCallback(imageList);

            this.state.imageUpload=false
            this.setState({imageUpload:this.state.imageUpload})
            }
            if(this.state.imageUpdate){
                if(addUpdateIndex!=undefined && addUpdateIndex!=null){
                imageList[addUpdateIndex[0]]['image_url']=this.state.images[addUpdateIndex[0]]['image_url']
                imageList[addUpdateIndex[0]]['image_id']=this.state.images[addUpdateIndex[0]]['image_id']
                imageList[addUpdateIndex[0]]['type']='update'

                postData(imageList[addUpdateIndex[0]], addUpdateIndex[0])
                console.log(imageList,addUpdateIndex,'saurav thakur')
            }
              this.setState({images:imageList})
                this.props.parentCallback(imageList);
                this.state.imageUpdate=false
                this.setState({imageUpdate:this.state.imageUpdate})

            }
              if(this.state.imageDelete){
              this.setState({images:imageList})
                this.props.parentCallback(imageList);
                this.state.imageDelete=false
                this.setState({imageDelete:this.state.imageDelete})
            }
          
           
          };
        
        const deleteImage=async (image,index)=>{
            console.log(image,index)
                if(index!=undefined && index!=null && image!=null && image !=undefined){
                image['type']='delete'
                postData(image, index)
                console.log(image,index,'saurav thakur')
                }
            //   this.setState({images:imageList})
            //     this.props.parentCallback(imageList);
                // this.state.imageDelete=false
                // this.setState({imageDelete:this.state.imageDelete})
        }
        return( 
        <FormGroup>
        <Label for="input-2">
            Image
        </Label>
                <ImageUploading
                multiple
                value={this.state.images}
                onChange={onChange}
                maxNumber={this.state.maxNumberOfImage}
                dataURLKey="data_url"
            >
                {({
                imageList,
                onImageUpload,
                onImageRemoveAll,
                onImageUpdate,
                onImageRemove,
                isDragging,
                dragProps,
                }) => (
                // write your building UI
                <div className="upload__image-wrapper">
                        <InputGroup >
                    <InputGroupAddon addonType="prepend">
                        <Button onClick={onImageRemoveAll} color="danger" outline>
                            Remove All
                        </Button>
                    </InputGroupAddon>
                    <InputGroupAddon addonType="prepend">
                        <Button 
                        color="secondary"
                        style={isDragging ? { color: 'red' } : undefined}
                        onClick={(data)=>{
                            this.state.imageUpload=true
                            this.setState({imageUpload:this.state.imageUpload})
                            onImageUpload(data)
                            }}
                        {...dragProps}
                            outline>
                        Click or Drop here
                        </Button>
                    </InputGroupAddon>
                    <InputGroupAddon addonType="append">
                        <i className="fa fa-fw fa-info-circle mr-2"></i>
                        All Images Accepted
                    </InputGroupAddon>
                </InputGroup>
                <Row style={{padding:'10px'}}>
                    {imageList.map((image, index) => (
                    <div key={index} className="image-item" style={{padding:'5px'}}>
                        <img src={image['data_url']} alt="" width="148" height="100" />
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                        <Button onClick={() => {
                            this.state.imageUpdate=true
                            this.setState({imageUpdate:this.state.imageUpdate})
                        onImageUpdate(index)
                        }
                        } color="secondary" outline>
                            Update 
                        </Button>
                        </InputGroupAddon>
                        <InputGroupAddon addonType="prepend">
                        <Button onClick={() => {
                            this.state.imageDelete=true
                            this.setState({imageDelete:this.state.imageDelete})
                            deleteImage(image,index)
                            onImageRemove(index)
                            }}color="danger" outline>
                            Remove 
                        </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </div>
                    ))}
                    </Row>
                </div>
                )}
            </ImageUploading>
        </FormGroup>
        )
    }
}