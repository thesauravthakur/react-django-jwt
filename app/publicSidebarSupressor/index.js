import React from 'react';
// import {NavbarOnly} from '../mainComponents/Layouts/NavbarOnly/NavbarOnly.js';
import PropTypes from 'prop-types';
import { withPageConfig } from
    '../components/Layout/withPageConfig';
import {
    Route,
    Switch,
    Redirect
} from 'react-router';
class MainPublicClass extends React.Component{

    static propTypes = {
        pageConfig: PropTypes.object
    };

    componentDidMount() {
        const { pageConfig } = this.props;
        
        pageConfig.setElementsVisibility({
            sidebarHidden: true,
            // navbarHidden:true
        });
    }

    componentWillUnmount() {
        const { pageConfig } = this.props;

        pageConfig.setElementsVisibility({
            sidebarHidden: false
        });
    }
    render(){
        return(
        <React.Fragment>{this.props.children}</React.Fragment>
        );
    }
}

const ExtendedMainPublicClass = withPageConfig(MainPublicClass);

export {
    ExtendedMainPublicClass as MainPublicClass
};