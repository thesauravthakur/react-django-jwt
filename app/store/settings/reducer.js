import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../store/utility";

export const initialState = {
  settingpost: {},
  setting: {},
  error: null,
  loading: false,
};

const getSettingStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true,

  });
};

const getSettingSuccess = (state, action) => {
  console.log(action.setting)
  return updateObject(state, {
    setting: action.setting,
    loading: false

  });
};

const getSettingFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const postSettingStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const postSettingSuccess = (state, action) => {
  return updateObject(state, {
    settingpost: action.settingpost
  });
};

const postSettingFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};




const authLogout = (state, action) => {
  return updateObject(state, {
    setting: null
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_SETTING_START:
      return getSettingStart(state, action);
    case actionTypes.GET_SETTING_SUCCESS:
      return getSettingSuccess(state, action);
    case actionTypes.GET_SETTING_FAIL:
      return getSettingFail(state, action);
    case actionTypes.POST_SETTING_START:
      return postSettingStart(state, action);
    case actionTypes.POST_SETTING_SUCCESS:
      return postSettingSuccess(state, action);
    case actionTypes.POST_SETTING_FAIL:
      return postSettingFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    default:
      return state;
  }
};

export default reducer;
