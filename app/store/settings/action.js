import axios from "axios";
import * as actionTypes from "./actionTypes";
import { mainLink } from '../utils';
import { logout } from '../actions/auth';
import axiosInstance from '../../axios'



export const getSettingStart = () => {
  return {
    type: actionTypes.GET_SETTING_START
  };
};

export const getSettingSuccess = setting => {
  return {
    type: actionTypes.GET_SETTING_SUCCESS,
    setting
  };
};

export const getSettingFail = error => {
  return {
    type: actionTypes.GET_SETTING_FAIL,
    error: error
  };
};






export const postSettingStart = () => {
  return {
    type: actionTypes.POST_SETTING_START
  };
};

export const postSettingSuccess = settingpost => {
  return {
    type: actionTypes.POST_SETTING_SUCCESS,
    settingpost
  };
};

export const postSettingFail = error => {
  return {
    type: actionTypes.POST_SETTING_FAIL,
    error: error
  };
};





export const getSetting = (token, onSuccess = null, onError = null) => {
  console.log(token)
console.log(localStorage.getItem('refresh_token'),'saurav thakur')

  return dispatch => {
    dispatch(getSettingStart());
    // axios.defaults.headers = {
    //   "Content-Type": "application/json",
    //   Authorization: `Token ${token}`,
    // };
    axiosInstance
      .get("/api/v1/settings/")
      .then(res => {
        console.log(res.data)
          if (typeof onSuccess == 'function') {
          onSuccess(res)
        }
        dispatch(getSettingSuccess(res.data));
      }
      ).catch(err => {
        if (typeof onError == 'function') {
          onError(err)
        }
        dispatch(getSettingFail(err));



      });
  };
};

export const postSetting = (data, token, onSuccess = null, onError = null) => {
  console.log(token)
  return dispatch => {
    dispatch(postSettingStart());
    axiosInstance
      .post("/api/v1/settings/", data)
      .then(res => {
        dispatch(postSettingSuccess(res.data));
        dispatch(getSetting(token));
        if (typeof onSuccess == 'function') {
          onSuccess()
        }

      }
      )
      .catch(err => {
        dispatch(postSettingFail(err));
        if (typeof onError == 'function') {
          onError()
        }

      });
  };
};