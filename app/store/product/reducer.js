import * as actionTypes from "./actionType";
import { updateObject } from "../../store1/utility";


export const initialState = {
	error: null,
	loading: null,
	product:[],
	productDetail:null,
}






const productGetStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const productGetSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		product: action.product,
	});
};
const productGetFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const productPostStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const productPostSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		productDetail: action.productDetail,
	});
};
const productPostFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const productEditStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const productEditSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		productDetail: action.productDetail,
	});
};
const productEditFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const productDetailStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const productDetailSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		productDetail: action.productDetail,
	});
};
const productDetailFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const productDeleteStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const productDeleteSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		productDetail: action.productDetail,
	});
};
const productDeleteFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const reducer = (state = initialState, action) => { 
	switch (action.type) { 
		case actionTypes.PRODUCT_GET_START:
			return productGetStart(state, action);
		case actionTypes.PRODUCT_GET_SUCCESS:
			return productGetSuccess(state, action);
		case actionTypes.PRODUCT_GET_FAIL:
			return productGetFail(state, action);






		case actionTypes.PRODUCT_POST_START:
			return productPostStart(state, action);
		case actionTypes.PRODUCT_POST_SUCCESS:
			return productPostSuccess(state, action);
		case actionTypes.PRODUCT_POST_FAIL:
			return productPostFail(state, action);






		case actionTypes.PRODUCT_EDIT_START:
			return productEditStart(state, action);
		case actionTypes.PRODUCT_EDIT_SUCCESS:
			return productEditSuccess(state, action);
		case actionTypes.PRODUCT_EDIT_FAIL:
			return productEditFail(state, action);






		case actionTypes.PRODUCT_DETAIL_START:
			return productDetailStart(state, action);
		case actionTypes.PRODUCT_DETAIL_SUCCESS:
			return productDetailSuccess(state, action);
		case actionTypes.PRODUCT_DETAIL_FAIL:
			return productDetailFail(state, action);






		case actionTypes.PRODUCT_DELETE_START:
			return productDeleteStart(state, action);
		case actionTypes.PRODUCT_DELETE_SUCCESS:
			return productDeleteSuccess(state, action);
		case actionTypes.PRODUCT_DELETE_FAIL:
			return productDeleteFail(state, action);



		default:
			return state;
	}
};
export default reducer;
