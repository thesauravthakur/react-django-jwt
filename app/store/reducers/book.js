import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

export const initialState = {
  error: null,
  loading: false,
  category: null,
  categorys: [],
  subcategory: {},
  subcategorys: [],
  book: null,
  books: [],
  edition: {},
  editions: [],
  detatilEdition: null,
  bookEntry: {},
  racks: [],
  printHtml: null,
  rack: {},
  room: {},
  booktype: null,
  booktypes: [],
  rooms: [],
  authors: [],
  bookEntrys: [],
  bookpostSuccess: false,
  publications: [],
  bookEntrysBatchResponse: {},
  batches:[]

};


export const clearbook = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    category: {},
    categorys: [],
    subcategory: {},
    subcategorys: [],
    book: {},
    bookEntrys: [],
    books: [],
    edition: {},
    booktype: {},
    booktypes: [],
    editions: [],
    detatilEdition: null,
    bookEntry: {},
    racks: [],
    rack: {},
    room: {},
    rooms: [],
    authors: [],
    bookpostSuccess: false,
    publications: [],
    bookEntrysBatchResponse: {},

  });
}



const bookEntryPostStart = (state, action) => {
  return updateObject(state, {
    error: null,
    printHtml:null,
    loading: true
  });
};

const bookEntryPostSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    bookEntry: action.bookEntry,
    printHtml: action.html
  });
};

const bookEntryPostFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};




const getBookDetailBookEntryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  })
}
const getBookDetailBookEntrySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    bookEntrys: action.bookEntrys,
  })
}
const getBookDetailBookEntryFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    bookEntrys: [],
    loading: false
  })
}



const getBookEntryBatchNumberStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  })
}
const getBookEntryBatchNumberSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    bookEntrysBatchResponse: action.bookEntrysBatchResponse,
  })
}
const getBookEntryBatchesSuccess = (state,action) => {
  return updateObject(state,{
    error:null,
    loading:false,
    batches: action.batches
  })
}
const getBookEntryBatchNumberFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  })
}








const bookRackGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookRackGetSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    racks: action.racks,
  });
};

const bookRackGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};





const bookRoomGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookRoomGetSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    rooms: action.rooms,
  });
};

const bookRoomGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};










const bookGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    books: action.books,
  });
};

const bookGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};




const bookPostStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    bookpostSuccess: true,
    book: action.book,
  });
};

const bookPostFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};





const bookEditStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    book: action.book,
  });
};

const bookEditFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};










const bookTypeGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookTypeGetSuccess = (state, action) => {
  console.log(action.booktypes)
  return updateObject(state, {
    error: null,
    loading: false,
    booktypes: action.booktypes,
  });
};

const bookTypeGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};






const bookTypePostStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookTypePostSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    bookpostSuccess: true,
    booktype: action.booktype,
  });
};

const bookTypePostFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};





const bookTypeEditStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookTypeEditSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    booktype: action.booktype,
  });
};

const bookTypeEditFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};























const bookPostEditionStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostEditionSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    postSuccess: true,
    loading: false,
    edition: action.edition,
  });
};

const bookPostEditionFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const bookGetEditionStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetEditionSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    editions: action.editions,
  });
};

const bookGetEditionFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const bookEditEditionStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditEditionSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    edition: action.edition,
  });
};

const bookEditEditionFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};




const bookDetailEditionSuccess = (state, action) => {
  console.log(state.detatilEdition)

  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    detatilEdition: action.detatilEdition,
  });
};
















const bookPostRackStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostRackSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    postSuccess: true,
    loading: false,
    rack: action.rack,
  });
};

const bookPostRackFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const bookDeleteRackFail = (state, action) => {
  console.log(action)
  return updateObject(state, {
    error: action.error.status,
    loading: false
  });
}

const bookGetRackStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetRackSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    racks: action.racks,
  });
};

const bookGetRackFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const bookEditRackStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditRackSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    rack: action.rack,
  });
};

const bookEditRackFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};














const bookPostRoomStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostRoomSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    postSuccess: true,
    loading: false,
    room: action.room,
  });
};

const bookPostRoomFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const bookDeleteRoomFail = (state, action) => {
  console.log(action)
  return updateObject(state, {
    error: action.error.status,
    loading: false
  });
}

const bookGetRoomStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetRoomSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    rooms: action.rooms,
  });
};

const bookGetRoomFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const bookEditRoomStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditRoomSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    room: action.room,
  });
};

const bookEditRoomFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};











const bookGetAuthorStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetAuthorSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    authors: action.authors,
  });
};

const bookGetAuthorFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};










const bookGetPublicationStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetPublicationSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    publications: action.publications,
  });
};

const bookGetPublicationFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


























const bookPostCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostCategorySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    postSuccess: true,
    loading: false,
    category: action.category,
  });
};

const bookPostCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};



const bookGetCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetCategorySuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    categorys: action.categorys,
  });
};

const bookGetCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const bookEditCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditCategorySuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    categorys: action.categorys,
  });
};

const bookEditCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


















const bookPostSubCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookPostSubCategorySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    postSuccess: true,
    loading: false,
    subcategory: action.subcategory,
  });
};

const bookPostSubCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const bookGetSubCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookGetSubCategorySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    subcategorys: action.subcategorys,
  });
};

const bookGetSubCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};


const bookEditSubCategoryStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const bookEditSubCategorySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    subcategory: action.subcategory,
  });
};

const bookEditSubCategoryFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};













const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.BOOK_POST_CATEGORY_START:
      return bookPostCategoryStart(state, action);
    case actionTypes.BOOK_POST_CATEGORY_SUCCESS:
      return bookPostCategorySuccess(state, action);
    case actionTypes.BOOK_POST_CATEGORY_FAIL:
      return bookPostCategoryFail(state, action);


    case actionTypes.BOOK_GET_CATEGORY_START:
      return bookGetCategoryStart(state, action);
    case actionTypes.BOOK_GET_CATEGORY_SUCCESS:
      return bookGetCategorySuccess(state, action);
    case actionTypes.BOOK_GET_CATEGORY_FAIL:
      return bookGetCategoryFail(state, action);


    case actionTypes.BOOK_EDIT_CATEGORY_START:
      return bookEditCategoryStart(state, action);
    case actionTypes.BOOK_EDIT_CATEGORY_SUCCESS:
      return bookEditCategorySuccess(state, action);
    case actionTypes.BOOK_EDIT_CATEGORY_FAIL:
      return bookEditCategoryFail(state, action);






    case actionTypes.BOOK_POST_RACK_START:
      return bookPostRackStart(state, action);
    case actionTypes.BOOK_POST_RACK_SUCCESS:
      return bookPostRackSuccess(state, action);
    case actionTypes.BOOK_POST_RACK_FAIL:
      return bookPostRackFail(state, action);


    case actionTypes.BOOK_GET_RACK_START:
      return bookGetRackStart(state, action);
    case actionTypes.BOOK_GET_RACK_SUCCESS:
      return bookGetRackSuccess(state, action);
    case actionTypes.BOOK_GET_RACK_FAIL:
      return bookGetRackFail(state, action);


    case actionTypes.BOOK_EDIT_RACK_START:
      return bookEditRackStart(state, action);
    case actionTypes.BOOK_EDIT_RACK_SUCCESS:
      return bookEditRackSuccess(state, action);
    case actionTypes.BOOK_EDIT_RACK_FAIL:
      return bookEditRackFail(state, action);

    case actionTypes.BOOK_DELETE_RACK_FAIL:
      return bookDeleteRackFail(state, action);






    case actionTypes.BOOK_POST_ROOM_START:
      return bookPostRoomStart(state, action);
    case actionTypes.BOOK_POST_ROOM_SUCCESS:
      return bookPostRoomSuccess(state, action);
    case actionTypes.BOOK_POST_ROOM_FAIL:
      return bookPostRoomFail(state, action);


    case actionTypes.BOOK_GET_ROOM_START:
      return bookGetRoomStart(state, action);
    case actionTypes.BOOK_GET_ROOM_SUCCESS:
      return bookGetRoomSuccess(state, action);
    case actionTypes.BOOK_GET_ROOM_FAIL:
      return bookGetRoomFail(state, action);


    case actionTypes.BOOK_EDIT_ROOM_START:
      return bookEditRoomStart(state, action);
    case actionTypes.BOOK_EDIT_ROOM_SUCCESS:
      return bookEditRoomSuccess(state, action);
    case actionTypes.BOOK_EDIT_ROOM_FAIL:
      return bookEditRoomFail(state, action);

    case actionTypes.BOOK_DELETE_ROOM_FAIL:
      return bookDeleteRoomFail(state, action);







    case actionTypes.BOOK_POST_EDITION_START:
      return bookPostEditionStart(state, action);
    case actionTypes.BOOK_POST_EDITION_SUCCESS:
      return bookPostEditionSuccess(state, action);
    case actionTypes.BOOK_POST_EDITION_FAIL:
      return bookPostEditionFail(state, action);


    case actionTypes.BOOK_GET_EDITION_START:
      return bookGetEditionStart(state, action);
    case actionTypes.BOOK_GET_EDITION_SUCCESS:
      return bookGetEditionSuccess(state, action);
    case actionTypes.BOOK_GET_EDITION_FAIL:
      return bookGetEditionFail(state, action);


    case actionTypes.BOOK_EDIT_EDITION_START:
      return bookEditEditionStart(state, action);
    case actionTypes.BOOK_EDIT_EDITION_SUCCESS:
      return bookEditEditionSuccess(state, action);
    case actionTypes.BOOK_EDIT_EDITION_FAIL:
      return bookEditEditionFail(state, action);









    case actionTypes.BOOK_ENTRY_POST_START:
      return bookEntryPostStart(state, action);
    case actionTypes.BOOK_ENTRY_POST_SUCCESS:
      return bookEntryPostSuccess(state, action);
    case actionTypes.BOOK_ENTRY_POST_FAIL:
      return bookEntryPostFail(state, action);

    case actionTypes.GET_BOOK_BOOKENTRY_DETAIL_START:
      return getBookDetailBookEntryStart(state, action);
    case actionTypes.GET_BOOK_BOOKENTRY_DETAIL_SUCCESS:
      return getBookDetailBookEntrySuccess(state, action);
    case actionTypes.GET_BOOK_BOOKENTRY_DETAIL_FAIL:
      return getBookDetailBookEntryFail(state, action)

    case actionTypes.GET_BOOKENTRY_BATCH_START:
      return getBookEntryBatchNumberStart(state, action);
    case actionTypes.GET_BOOKENTRY_BATCH_SUCCESS:
      return getBookEntryBatchNumberSuccess(state, action);
    case actionTypes.GET_BOOKENTRY_BATCH_FAIL:
      return getBookEntryBatchNumberFail(state, action)
    case actionTypes.GET_BOOKENTRY_BATCHES_SUCCESS:
      return getBookEntryBatchesSuccess(state, action);






    case actionTypes.BOOK_RACK_GET_START:
      return bookRackGetStart(state, action);
    case actionTypes.BOOK_RACK_GET_SUCCESS:
      return bookRackGetSuccess(state, action);
    case actionTypes.BOOK_RACK_GET_FAIL:
      return bookRackGetFail(state, action);






    case actionTypes.BOOK_ROOM_GET_START:
      return bookRoomGetStart(state, action);
    case actionTypes.BOOK_ROOM_GET_SUCCESS:
      return bookRoomGetSuccess(state, action);
    case actionTypes.BOOK_ROOM_GET_FAIL:
      return bookRoomGetFail(state, action);




    case actionTypes.BOOKTYPE_GET_START:
      return bookTypeGetStart(state, action);
    case actionTypes.BOOKTYPE_GET_SUCCESS:
      return bookTypeGetSuccess(state, action);
    case actionTypes.BOOKTYPE_GET_FAIL:
      return bookTypeGetFail(state, action);



    case actionTypes.BOOKTYPE_POST_START:
      return bookTypePostStart(state, action);
    case actionTypes.BOOKTYPE_POST_SUCCESS:
      return bookTypePostSuccess(state, action);
    case actionTypes.BOOKTYPE_POST_FAIL:
      return bookTypePostFail(state, action);


    case actionTypes.BOOKTYPE_EDIT_START:
      return bookTypeEditStart(state, action);
    case actionTypes.BOOKTYPE_EDIT_SUCCESS:
      return bookTypeEditSuccess(state, action);
    case actionTypes.BOOKTYPE_EDIT_FAIL:
      return bookTypeEditFail(state, action);




    case actionTypes.BOOK_POST_START:
      return bookPostStart(state, action);
    case actionTypes.BOOK_POST_SUCCESS:
      return bookPostSuccess(state, action);
    case actionTypes.BOOK_POST_FAIL:
      return bookPostFail(state, action);


    case actionTypes.BOOK_GET_START:
      return bookGetStart(state, action);
    case actionTypes.BOOK_GET_SUCCESS:
      return bookGetSuccess(state, action);
    case actionTypes.BOOK_GET_FAIL:
      return bookGetFail(state, action);





    case actionTypes.BOOK_EDIT_START:
      return bookEditStart(state, action);
    case actionTypes.BOOK_EDIT_SUCCESS:
      return bookEditSuccess(state, action);
    case actionTypes.BOOK_EDIT_FAIL:
      return bookEditFail(state, action);







    case actionTypes.BOOK_POST_SUBCATEGORY_START:
      return bookPostSubCategoryStart(state, action);
    case actionTypes.BOOK_POST_SUBCATEGORY_SUCCESS:
      return bookPostSubCategorySuccess(state, action);
    case actionTypes.BOOK_POST_SUBCATEGORY_FAIL:
      return bookPostSubCategoryFail(state, action);



    case actionTypes.BOOK_GET_SUBCATEGORY_START:
      return bookGetSubCategoryStart(state, action);
    case actionTypes.BOOK_GET_SUBCATEGORY_SUCCESS:
      return bookGetSubCategorySuccess(state, action);
    case actionTypes.BOOK_GET_SUBCATEGORY_FAIL:
      return bookGetSubCategoryFail(state, action);




    case actionTypes.BOOK_EDIT_SUBCATEGORY_START:
      return bookEditSubCategoryStart(state, action);
    case actionTypes.BOOK_EDIT_SUBCATEGORY_SUCCESS:
      return bookEditSubCategorySuccess(state, action);
    case actionTypes.BOOK_EDIT_SUBCATEGORY_FAIL:
      return bookEditSubCategoryFail(state, action);


    case actionTypes.BOOK_DETAIL_EDITION_SUCCESS:
      return bookDetailEditionSuccess(state, action);


    case actionTypes.BOOK_GET_AUTHOR_START:
      return bookGetAuthorStart(state, action);
    case actionTypes.BOOK_GET_AUTHOR_SUCCESS:
      return bookGetAuthorSuccess(state, action);
    case actionTypes.BOOK_GET_AUTHOR_FAIL:
      return bookGetAuthorFail(state, action);


    case actionTypes.BOOK_GET_PUBLICATION_START:
      return bookGetPublicationStart(state, action);
    case actionTypes.BOOK_GET_PUBLICATION_SUCCESS:
      return bookGetPublicationSuccess(state, action);
    case actionTypes.BOOK_GET_PUBLICATION_FAIL:
      return bookGetPublicationFail(state, action);

    case actionTypes.AUTH_LOGOUT:
      return clearbook(state, action);


    default:
      return state;
  }
};

export default reducer;
