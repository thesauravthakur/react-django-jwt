import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

export const initialState = {
  error: null,
  loading: false,
  membertype: {},
  membertypes: [],
  permissions: [],
  member: null,
  members: [],
};


export const clearmember = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    membertype: {},
    permissions: [],
    membertypes: [],
    member: null,
    members: [],

  });
}



const permissionGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const permissionGetSuccess = (state, action) => {
  console.log(action.permissions)
  return updateObject(state, {
    error: null,
    loading: false,
    permissions: action.permissions,
  });
};

const permissionGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};














const memberGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberGetSuccess = (state, action) => {
  console.log(action.members)
  return updateObject(state, {
    error: null,
    loading: false,
    members: action.members,
  });
};

const memberGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};






const memberPostStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberPostSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    memberpostSuccess: true,
    member: action.member,
  });
};

const memberPostFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};





const memberEditStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberEditSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    member: action.member,
  });
};

const memberEditFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};






const memberTypeGetStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberTypeGetSuccess = (state, action) => {
  console.log(action.membertypes)
  return updateObject(state, {
    error: null,
    loading: false,
    membertypes: action.membertypes,
  });
};

const memberTypeGetFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};






const memberTypePostStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberTypePostSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    memberpostSuccess: true,
    membertype: action.membertype,
  });
};

const memberTypePostFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};





const memberTypeEditStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const memberTypeEditSuccess = (state, action) => {
  console.log(action.categorys)
  return updateObject(state, {
    error: null,
    loading: false,
    membertype: action.membertype,
  });
};

const memberTypeEditFail = (state, action) => {
  console.log(action.error)
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};









const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PERMISSION_GET_START:
      return permissionGetStart(state, action);
    case actionTypes.PERMISSION_GET_SUCCESS:
      return permissionGetSuccess(state, action);
    case actionTypes.PERMISSION_GET_FAIL:
      return permissionGetFail(state, action);


    case actionTypes.MEMBER_GET_START:
      return memberGetStart(state, action);
    case actionTypes.MEMBER_GET_SUCCESS:
      return memberGetSuccess(state, action);
    case actionTypes.MEMBER_GET_FAIL:
      return memberGetFail(state, action);



    case actionTypes.MEMBER_POST_START:
      return memberPostStart(state, action);
    case actionTypes.MEMBER_POST_SUCCESS:
      return memberPostSuccess(state, action);
    case actionTypes.MEMBER_POST_FAIL:
      return memberPostFail(state, action);


    case actionTypes.MEMBER_EDIT_START:
      return memberEditStart(state, action);
    case actionTypes.MEMBER_EDIT_SUCCESS:
      return memberEditSuccess(state, action);
    case actionTypes.MEMBER_EDIT_FAIL:
      return memberEditFail(state, action);

    case actionTypes.MEMBERTYPE_GET_START:
      return memberTypeGetStart(state, action);
    case actionTypes.MEMBERTYPE_GET_SUCCESS:
      return memberTypeGetSuccess(state, action);
    case actionTypes.MEMBERTYPE_GET_FAIL:
      return memberTypeGetFail(state, action);



    case actionTypes.MEMBERTYPE_POST_START:
      return memberTypePostStart(state, action);
    case actionTypes.MEMBERTYPE_POST_SUCCESS:
      return memberTypePostSuccess(state, action);
    case actionTypes.MEMBERTYPE_POST_FAIL:
      return memberTypePostFail(state, action);


    case actionTypes.MEMBERTYPE_EDIT_START:
      return memberTypeEditStart(state, action);
    case actionTypes.MEMBERTYPE_EDIT_SUCCESS:
      return memberTypeEditSuccess(state, action);
    case actionTypes.MEMBERTYPE_EDIT_FAIL:
      return memberTypeEditFail(state, action);


    case actionTypes.AUTH_LOGOUT:
      return clearmember(state, action);


    default:
      return state;
  }
};

export default reducer;
