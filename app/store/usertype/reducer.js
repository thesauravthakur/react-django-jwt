import * as actionTypes from "./actionType";
import { updateObject } from "../utility";


export const initialState = {
	error: null,
	loading: null,
	usertype:[],
	usertypeDetail:null,
}






const usertypeGetStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const usertypeGetSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		usertype: action.usertype,
	});
};
const usertypeGetFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const usertypePostStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const usertypePostSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		usertypeDetail: action.usertypeDetail,
	});
};
const usertypePostFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const usertypeEditStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const usertypeEditSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		usertypeDetail: action.usertypeDetail,
	});
};
const usertypeEditFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const usertypeDetailStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const usertypeDetailSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		usertypeDetail: action.usertypeDetail,
	});
};
const usertypeDetailFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const usertypeDeleteStart = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: true,
	});
};
const usertypeDeleteSuccess = (state, action) => {
	return updateObject(state, {
		error: null,
		loading: false,
		usertypeDetail: action.usertypeDetail,
	});
};
const usertypeDeleteFail = (state, action) => {
	return updateObject(state, {
		error: action.error,
		loading: false,
	});
};






const reducer = (state = initialState, action) => { 
	switch (action.type) { 
		case actionTypes.USERTYPE_GET_START:
			return usertypeGetStart(state, action);
		case actionTypes.USERTYPE_GET_SUCCESS:
			return usertypeGetSuccess(state, action);
		case actionTypes.USERTYPE_GET_FAIL:
			return usertypeGetFail(state, action);






		case actionTypes.USERTYPE_POST_START:
			return usertypePostStart(state, action);
		case actionTypes.USERTYPE_POST_SUCCESS:
			return usertypePostSuccess(state, action);
		case actionTypes.USERTYPE_POST_FAIL:
			return usertypePostFail(state, action);






		case actionTypes.USERTYPE_EDIT_START:
			return usertypeEditStart(state, action);
		case actionTypes.USERTYPE_EDIT_SUCCESS:
			return usertypeEditSuccess(state, action);
		case actionTypes.USERTYPE_EDIT_FAIL:
			return usertypeEditFail(state, action);






		case actionTypes.USERTYPE_DETAIL_START:
			return usertypeDetailStart(state, action);
		case actionTypes.USERTYPE_DETAIL_SUCCESS:
			return usertypeDetailSuccess(state, action);
		case actionTypes.USERTYPE_DETAIL_FAIL:
			return usertypeDetailFail(state, action);






		case actionTypes.USERTYPE_DELETE_START:
			return usertypeDeleteStart(state, action);
		case actionTypes.USERTYPE_DELETE_SUCCESS:
			return usertypeDeleteSuccess(state, action);
		case actionTypes.USERTYPE_DELETE_FAIL:
			return usertypeDeleteFail(state, action);



		default:
			return state;
	}
};
export default reducer;
