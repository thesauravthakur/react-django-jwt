import axiosInstance from "../../axios.js";
import * as actionTypes from "./actionType";
import { mainLink } from "../utils";



export const usertypePostStart= () => {
	return {
		type: actionTypes.USERTYPE_POST_START
		};
};


export const usertypePostSuccess= (usertypeDetail) => {
	return {
		type: actionTypes.USERTYPE_POST_SUCCESS,
		 usertypeDetail: usertypeDetail
		};
};


export const usertypePostFail= (error) => {
	return {
		type: actionTypes.USERTYPE_POST_FAIL,
		 error: error
		};
};









export const usertypeGetStart= () => {
	return {
		type: actionTypes.USERTYPE_GET_START
		};
};


export const usertypeGetSuccess= (usertype) => {
	return {
		type: actionTypes.USERTYPE_GET_SUCCESS,
		 usertype: usertype
		};
};


export const usertypeGetFail= (error) => {
	return {
		type: actionTypes.USERTYPE_GET_FAIL,
		 error: error
		};
};









export const usertypeEditStart= () => {
	return {
		type: actionTypes.USERTYPE_EDIT_START
		};
};


export const usertypeEditSuccess= (usertypeDetail) => {
	return {
		type: actionTypes.USERTYPE_EDIT_SUCCESS,
		 usertypeDetail: usertypeDetail
		};
};


export const usertypeEditFail= (error) => {
	return {
		type: actionTypes.USERTYPE_EDIT_FAIL,
		 error: error
		};
};









export const usertypeDeleteStart= () => {
	return {
		type: actionTypes.USERTYPE_DELETE_START
		};
};


export const usertypeDeleteSuccess= (usertypeDetail) => {
	return {
		type: actionTypes.USERTYPE_DELETE_SUCCESS,
		 usertypeDetail: usertypeDetail
		};
};


export const usertypeDeleteFail= (error) => {
	return {
		type: actionTypes.USERTYPE_DELETE_FAIL,
		 error: error
		};
};









export const usertypeDetailStart= () => {
	return {
		type: actionTypes.USERTYPE_DETAIL_START
		};
};


export const usertypeDetailSuccess= (usertypeDetail) => {
	return {
		type: actionTypes.USERTYPE_DETAIL_SUCCESS,
		 usertypeDetail: usertypeDetail
		};
};


export const usertypeDetailFail= (error) => {
	return {
		type: actionTypes.USERTYPE_DETAIL_FAIL,
		 error: error
		};
};









export const postusertype = (usertype, onSuccess = null, onError = null) => {
	return dispatch => {
		// console.log(usertype,'saurav thakurji and team')
		dispatch(usertypePostStart());
		axiosInstance
			 .post(`/api/v1/usertype/`,usertype) 
			 .then(res => {
					 console.log(res,'team')
					 console.log(onSuccess=='function','team')

				 if (typeof onSuccess == 'function') {
					 console.log(res,'team')
					onSuccess(res)
				}
				 dispatch(usertypePostSuccess(res.data));
			}
			 ).catch(err => {
				 dispatch(usertypePostFail(err));
				 if (typeof onError == 'function') {
					onError(err)
				}
			 });
	 };
 };









export const getusertype = (onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(usertypeGetStart());
		axiosInstance
			 .get(`/api/v1/usertype/`) 
			 .then(res => {
				 dispatch(usertypeGetSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess(res)
				}
			}
			 ).catch(err => {
				 dispatch(usertypeGetFail(err));
				 if (typeof onError == 'function') {
					onError(err)
				}
			 });
	 };
 };









export const editusertype = (usertype,token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(usertypeEditStart());
		axiosInstance
			 .put(`/api/v1/usertype/${id}/`,usertype) 
			 .then(res => {
				 dispatch(usertypeEditSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(usertypeEditFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const getDetailusertype = (token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(usertypeDetailStart());
		axiosInstance
			 .get(`/api/v1/usertype/${id}/`) 
			 .then(res => {
				 dispatch(usertypeDetailSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(usertypeDetailFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const deleteusertype = (token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(usertypeDeleteStart());
		axiosInstance
			 .delete(`/api/v1/usertype/${id}/`) 
			 .then(res => {
				 dispatch(usertypeDeleteSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(usertypeDeleteFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









