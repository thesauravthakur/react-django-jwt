import React from 'react';
import faker from 'faker/locale/en_US';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {useSelector,useDispatch} from 'react-redux';
import { authLogin,logout } from '../../../store/actions/auth';

import { 
    DropdownMenu,
    DropdownItem,
    NavLink
} from './../../../components';

const DropdownProfile = (props) => {
    const dispatch=useDispatch()
    return (
    <React.Fragment>
        <DropdownMenu right={ props.right } >
            <DropdownItem header>
                { faker.name.firstName() } { faker.name.lastName() }
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem tag={ Link } to="/public/profile-details">
                My Profile
            </DropdownItem>
            <DropdownItem tag={ Link } to="/public/settings-edit">
                Settings
            </DropdownItem>
            <DropdownItem tag={ Link } to="/public/billing-edit">
                Billings
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem tag={ Link } to={`${'/admin/pages/login'}`}>
            <NavLink onClick={()=>dispatch(logout())} tag={ Link }  to={`${'/admin/pages/login'}`}>
                    <i  className="fa fa-fw fa-sign-out mr-2"></i>
                        Sign Out
            </NavLink>
            </DropdownItem>
        </DropdownMenu>
    </React.Fragment>
)
    }
DropdownProfile.propTypes = {
    position: PropTypes.string,
    right: PropTypes.bool
};
DropdownProfile.defaultProps = {
    position: ""
};

export { DropdownProfile };
