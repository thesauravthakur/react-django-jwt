import React from 'react';
import { NavLink as RouterNavLink } from 'react-router-dom';
import { 
    Nav,
    NavItem,
    NavLink
} from './../../../components';

const ProfileLeftNav = () => (
    <React.Fragment>
        { /* START Left Nav  */}
        <div className="mb-4">
            <Nav pills vertical>
                <NavItem>
                {/* <NavItem>
                    <NavLink tag={ RouterNavLink } to="/admin/apps/billing-edit">
                        Billing Edit
                    </NavLink>
                </NavItem> */}
                {/* <NavItem>
                    <NavLink tag={ RouterNavLink } to="/admin/apps/sessions-edit">
                        Sessions Edit
                    </NavLink>
                </NavItem> */}
                    <NavLink tag={ RouterNavLink } to="/admin/profile-edit">
                        Profile Edit
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={ RouterNavLink } to="/admin/account-edit">
                        Account Edit
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={ RouterNavLink } to="/admin/settings-edit">
                        Settings Edit
                    </NavLink>
                </NavItem>
            </Nav>
        </div>
        { /* END Left Nav  */}
    </React.Fragment>
)

export { ProfileLeftNav };
