import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withPageConfig } from
    './../../../components/Layout/withPageConfig';
import {
    Container,
} from './../../../components';

class NavbarOnly extends React.Component {
    static propTypes = {
        pageConfig: PropTypes.object
    };

    componentDidMount() {
        const { pageConfig } = this.props;
        
        pageConfig.setElementsVisibility({
            sidebarHidden: true
        });
    }

    componentWillUnmount() {
        const { pageConfig } = this.props;

        pageConfig.setElementsVisibility({
            sidebarHidden: false
        });
    }

    render() {
        return (
            <Container>
                <p className="mb-5 mt-3">
                    Welcome to the <b>&quot;Airframe&quot;</b> Admin Dashboard Theme based on <a href="https://getbootstrap.com" className="text-primary" target="_blank" rel="noopener noreferrer">Bootstrap 4.x</a> version for React called&nbsp;
                    <a href="https://reactstrap.github.io" className="text-primary" target="_blank" rel="noopener noreferrer">reactstrap</a> - easy to use React Bootstrap 4 components compatible with React 16+.
                </p>

                <section className="mb-5">
                    <h6>
                        Layouts for this framework:
                    </h6>
                    <ul className="pl-3">
                        <li>
                            <Link to="/public/layouts/navbar" className="text-primary">Navbar</Link>
                        </li>
                        <li>
                            <Link to="/admin/layouts/sidebar" className="text-primary">Sidebar</Link>
                        </li>
                        <li>
                            <Link to="/admin/layouts/sidebar-with-navbar" className="text-primary">Sidebar with Navbar</Link>
                        </li>
                    </ul>
                </section>

 
            </Container>
        );
    }
}

const ExtendedNavbarOnly = withPageConfig(NavbarOnly);

export {
    ExtendedNavbarOnly as NavbarOnly
};
