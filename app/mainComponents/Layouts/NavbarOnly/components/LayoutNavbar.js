import React from 'react';
import { Link } from 'react-router-dom';
import {
    Button,
    DropdownToggle,
    Nav,
    NavItem,
    NavLink,
    Avatar,
    AvatarAddOn,
    Navbar,
    NavbarToggler,
    UncontrolledDropdown,
    ThemeConsumer,
} from './../../../../components';
import { randomAvatar } from './../../../../utilities';

import { NavbarActivityFeed } from
    './../../../../layout/components/NavbarActivityFeed';
import { NavbarMessages } from
    './../../../../layout/components/NavbarMessages';
import { NavbarUser } from
    './../../../../layout/components/NavbarUser';
import { DropdownProfile } from
    './../../../components/Dropdowns/DropdownProfile';
import { NavbarNavigation } from
    './../../../components/Navbars/NavbarNavigation';
import { LogoThemed } from
    './../../../components/LogoThemed/LogoThemed';

export const LayoutNavbar = () => (
    <React.Fragment>
        

        <Navbar light shadow expand="lg" className="py-3 bg-white">
            <h1 className="mb-0 h4">
                Navbar Only
            </h1>
            
            <ThemeConsumer>
            {
                ({ color }) => (
                    <Button color={ color } className="px-4 my-sm-0">
                        Download <i className="fa ml-1 fa-fw fa-download"></i>
                    </Button>
                )
            }
            </ThemeConsumer>
        </Navbar>
    </React.Fragment>
);
