import React from 'react';
import { Link } from 'react-router-dom';
import MaskedInput from 'react-text-mask'
import {
  emailMask
} from 'text-mask-addons';
import { connect } from "react-redux";
import { authLogin,authSignUp } from '../../../store/actions/auth';

import { compose } from 'redux';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Form,
  FormGroup,
  FormText,
  Input,
  CustomInput,
  Button,
  Label,
  EmptyLayout,
  ThemeConsumer
} from './../../../components';

import { HeaderAuth } from "../../components/Pages/HeaderAuth";
import { FooterAuth } from "../../components/Pages/FooterAuth";
import { ToastContainer, toast } from 'react-toastify';
const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});


export const validate = (values, requiredfields) => {
  const errors = {}
  const requiredFields = requiredfields
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}
class Login extends React.Component {
  state = {
    username: '',
    password: '',
    email: '',
    redirect: null,
    showPassword: false,
    hasErrorMessage: false,
    isErrorDialogPopUp: false,
    ErrorMessage: '',
    invalidEmail: false,
    type: 'default',
    login:true,

  }
  render() {
    const handelSubmit = (e) => {
      e.preventDefault();
      const { username, password,email } = this.state
      if(this.state.login){
        console.log('saurav thakurji',email,password)
      this.props.onAuthLogin(email, password,()=>{
      // this.state.isErrorDialogPopUp = true
      // this.state.ErrorMessage = `Welome to the system`
        // this.setState({isErrorDialogPopUp:this.state.isErrorDialogPopUp,ErrorMessage:this.state.ErrorMessage})
        this.props.history.push("/admin/dashboard");

      },
      ()=>{
      this.state.isErrorDialogPopUp = true
      this.state.ErrorMessage = `Credentials Could Not be Verified`
        this.setState({isErrorDialogPopUp:this.state.isErrorDialogPopUp,ErrorMessage:this.state.ErrorMessage})

      }
      )
      }
      else{
      this.props.authSignUp(email,username,password,()=>{
      this.setState({login:true})
      this.state.isErrorDialogPopUp = true
      this.state.ErrorMessage = `Your Account has been created Please Login`
        this.setState({isErrorDialogPopUp:this.state.isErrorDialogPopUp,ErrorMessage:this.state.ErrorMessage})

      },
      (err)=>{
        // console.log(err.message)
        // console.log(err.)
        this.state.isErrorDialogPopUp = true
        this.state.ErrorMessage = `Credentials Could Not be Verified`
        this.setState({isErrorDialogPopUp:this.state.isErrorDialogPopUp,ErrorMessage:this.state.ErrorMessage})
      }
      )
      }

    }

    const { classes } = this.props;
    const { error } = this.props;
    const { successAuth } = this.props;
    
    return (
      <EmptyLayout>
        <ToastContainer
        
        />
        <EmptyLayout.Section center>
          { /* START Header */}
          <HeaderAuth
            title="Sign In to Application"
          />
          { /* END Header */}
          { /* START Form */}
          <Form className="mb-3">

              {
                !this.state.login &&
                <FormGroup>
              <Label for="email">
                Username
            </Label>
              <Input
                placeholder='Username'
                autoFocus
                id="username"
                onChange={(e) => { this.setState({ username: e.target.value }) }}
              />
            </FormGroup>
                
              }

            <FormGroup>
              <Label for="email">
                Email
            </Label>
              <Input
                mask={emailMask}
                placeholder='iamthakursaurav@gmail.com'
                tag={MaskedInput}
                autoFocus
                id="email"
                onChange={(e) => { this.setState({ email: e.target.value }) }}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">
                Password
                    </Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="Password..."
                className="bg-white"
                value={this.state.password}
                onChange={(e) => { this.setState({ password: e.target.value }) }}

              />
            </FormGroup>
            <FormGroup>
              <CustomInput type="checkbox" id="rememberPassword" label="Remember Password" inline />
            </FormGroup>
            <ThemeConsumer>
              {
                ({ color }) => (
                  <Button color={color} block tag={Link} onClick={(e) => handelSubmit(e)}>
                    Sign In
                  </Button>
                )
              }
            </ThemeConsumer>
          </Form>
          { /* END Form */}
          { /* START Bottom Links */}
          <div className="d-flex mb-5">
            <Link to="/admin/pages/forgotpassword" className="text-decoration-none">
              Forgot Password
                </Link>
                <div
                className="ml-auto text-decoration-none"
                style={{cursor:'pointer'}}
                onClick={() => { this.setState({ login: !this.state.login }) }}
                >

            {/* <Link to="/admin/pages/register" className="ml-auto text-decoration-none"> */}
              Register
                {/* </Link> */}
                </div>
          </div>
          { /* END Bottom Links */}
          { /* START Footer */}
          <FooterAuth />
          { /* END Footer */}
        </EmptyLayout.Section>
      </EmptyLayout>

    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    loading: state.auth.loading,
    error: state.auth.error,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuthLogin: (email, password, onSuccess , onError ) =>
      dispatch(authLogin(email, password, onSuccess , onError )),
    authSignUp:(email,username,password, onSuccess , onError)=>dispatch(authSignUp(email,username,password, onSuccess , onError ))
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Login);
