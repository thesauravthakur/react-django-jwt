import React, { Component } from "react";

class App extends Component {

  constructor(props) {
    super();
    this.state = {
      foo: "bar",
      resumeData: {},
      sharedData: {},
    };
  }


  render() {
    return (
     <div style={{padding:'5px'}}>
    <span style={{fontFamily:'Agustina',fontWeight:'bold',padding:'0 10px',fontSize:'27px'}}>
     <span>{' < '}</span> {'Saurav Thakur '}    <span>{' /> '}</span> 
    </span>
    <div style={{textAlign:'center',fontSize:'30px'}}>
        Saurav Thakur
    </div>
     </div>
    );
  }
}

export default App;
