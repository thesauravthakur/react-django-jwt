import React from 'react';

import { connect } from "react-redux";
import { authLogin,logout } from '../../../store/actions/auth';
import SettingsEdit from '../../../mainComponents/Apps/SettingsEdit';

class Index extends React.Component {
    
    state={

    }
    render() {

        return (
            <div>
              <SettingsEdit data={this.props}/>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
      state:state,
      token: state.auth.token,
      loading: state.auth.loading,
      error: state.auth.error,
      successAuth: state.auth.successAuth
    };
  };
const mapDispatchToProps = dispatch => {
    return {
      onAuthLogin: (username, password) =>
        dispatch(authLogin(username, password)),
        logout:()=>dispatch(logout())
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(Index);
