import React from 'react';

import { 
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    FormFeedback,
    Badge,
    CustomInput,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText,
    Button,
    InputGroup,
    InputGroupAddon
} from '../../../components';

import { HeaderMain } from "../../../mainComponents/components/HeaderMain";
import { HeaderDemo } from "../../../mainComponents/components/HeaderDemo";
import ImageUploading from 'react-images-uploading';
export default class ExampleForm extends React.Component{

    state={
        formTitle:'Create Example',
        images:[],
        maxNumber:10
    }

    render(){
        const onChange = (imageList, addUpdateIndex) => {
            // data for submit
            console.log(imageList, addUpdateIndex);
            this.setState({images:imageList})
          };

        return(
            <React.Fragment>
            <Container>
           
                    <Card className="mb-3">
                        <CardBody>
                            <CardTitle tag="h6" className="mb-4">
                                {this.state.formTitle}
                               
                            </CardTitle>
                                <Row>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="input-2">
                                        Name
                                    </Label>
                                    <Input 
                                        type="text" 
                                        name="" 
                                        id="input-2" 
                                        placeholder="Enter Name..." 
                                    />
                                    </FormGroup>
                                    </Col>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="inputPassword-2">
                                    Price
                                    </Label>
                                    <Input 
                                        type="number" 
                                        name="test" 
                                        id="test-2" 
                                        placeholder="Price..." 
                                    />
                                </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="operatingSystem" className="pt-0">
                                    Satisfaction
                                    </Label>
                                    <div>
                                        <CustomInput 
                                            type="radio" 
                                            id="1"
                                            name="operatingSystem"
                                            label="1" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="2" 
                                            name="operatingSystem"
                                            label="2" 
                                            inline
                                        /> 
                                        <CustomInput 
                                            type="radio" 
                                            id="3" 
                                            name="operatingSystem"
                                            label="3" 
                                            inline
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="4" 
                                            name="operatingSystem"
                                            label="3" 
                                            inline
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="5" 
                                            name="operatingSystem"
                                            label="5" 
                                            inline
                                        />
                                        
                                        
                                    </div>
                                </FormGroup>
                                    </Col>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="input-2">
                                    Attribute 4
                                    </Label>
                                    <Input 
                                        type="text" 
                                        name="" 
                                        id="input-2" 
                                        placeholder="Enter Name..." 
                                    />
                                    </FormGroup>
                                    </Col>
                                </Row>
                          
                               <Row>
                                   <Col lg={6}>
                                   <FormGroup>
                                    <Label for="input-2">
                                    Attribute  5
                                    </Label>
                                    <Input 
                                        type="text" 
                                        name="" 
                                        id="input-2" 
                                        placeholder="Enter Name..." 
                                    />
                                    </FormGroup>
                                   </Col>
                                   <Col lg={6}>
                                   <FormGroup>
                                    <Label for="country-selector-2">
                                    Attribute 6
                                    </Label>
                                    <CustomInput 
                                        type="select" 
                                        name="customSelect" 
                                        id="country-selector-2"  
                                    >
                                        <option value="">Select Country...</option>
                                        <option>United States of America (US)</option>
                                        <option>United Kingdom (UK)</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                        <option>Other...</option>
                                    </CustomInput>
                                </FormGroup>
                                   </Col>
                               </Row>
                               
                                <Row>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="addCv2">
                                        File 
                                    </Label>
                                    <CustomInput type="file" id="addCv2" name="customFile" label="Choose file..." />
                                    <FormText color="muted">
                                        Accepted formats: pdf, doc, txt. 
                                    </FormText>
                                </FormGroup>
                                    </Col>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="message-2">
                                        Message
                                    </Label>
                                        <Input 
                                            type="textarea" 
                                            name="text" 
                                            id="message-2" 
                                            placeholder="Enter Your Message..." 
                                            className="mb-2"
                                        />
                                        
                                </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                <Col >
                                    <FormGroup>
                                    <Label for="input-2">
                                        Image
                                    </Label>
                                         <ImageUploading
                                            multiple
                                            value={this.state.images}
                                            onChange={onChange}
                                            maxNumber={this.state.maxNumber}
                                            dataURLKey="data_url"
                                        >
                                            {({
                                            imageList,
                                            onImageUpload,
                                            onImageRemoveAll,
                                            onImageUpdate,
                                            onImageRemove,
                                            isDragging,
                                            dragProps,
                                            }) => (
                                            // write your building UI
                                            <div className="upload__image-wrapper">
                                                    <InputGroup >
                                                <InputGroupAddon addonType="prepend">
                                                    <Button onClick={onImageRemoveAll} color="danger" outline>
                                                        Remove All
                                                    </Button>
                                                </InputGroupAddon>
                                                <InputGroupAddon addonType="prepend">
                                                    <Button 
                                                    color="secondary"
                                                    style={isDragging ? { color: 'red' } : undefined}
                                                    onClick={onImageUpload}
                                                    {...dragProps}
                                                     outline>
                                                    Click or Drop here
                                                    </Button>
                                                </InputGroupAddon>
                                                <InputGroupAddon addonType="append">
                                                    <i className="fa fa-fw fa-info-circle mr-2"></i>
                                                    All Images Accepted
                                                </InputGroupAddon>
                                            </InputGroup>
                                            <Row style={{padding:'10px'}}>
                                                {imageList.map((image, index) => (
                                                <div key={index} className="image-item" style={{padding:'5px'}}>
                                                    <img src={image['data_url']} alt="" width="148" height="100" />
                                                    <InputGroup>
                                                    <InputGroupAddon addonType="prepend">
                                                    <Button onClick={() => onImageUpdate(index)} color="secondary" outline>
                                                        Update 
                                                    </Button>
                                                    </InputGroupAddon>
                                                    <InputGroupAddon addonType="prepend">
                                                    <Button onClick={() => onImageRemove(index)}color="danger" outline>
                                                        Remove 
                                                    </Button>
                                                    </InputGroupAddon>
                                                    </InputGroup>
                                                </div>
                                                ))}
                                                </Row>
                                            </div>
                                            )}
                                        </ImageUploading>
                                    </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg={6}>
                                    <CustomInput 
                                            type="checkbox" 
                                            id="iConfirm" 
                                            label="I confirm that I have read the Terms." 
                                            className="mb-3"
                                        />
                                        <Button color="primary">Save</Button>   
                                    </Col>
                                </Row>
                             
            
                            { /* END Form */}
                        </CardBody>
                    </Card>
              
            { /* END Section 1 */}
           
        </Container>
        
        </React.Fragment>
        )
    }
}