import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { Comparator, dateFilter } from 'react-bootstrap-table2-filter'
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import _ from 'lodash';
import faker from 'faker/locale/en_US';
import moment from 'moment';
import {
    Badge,
    Button,
    CustomInput,
    StarRating,
    ButtonGroup,
    InputGroup,
    InputGroupAddon
} from '../../../components';
import { CustomExportCSV } from '../../../mainComponents/Tables/ExtendedTable/components/CustomExportButton';
import { CustomSearch } from '../../../mainComponents/Tables/ExtendedTable/components//CustomSearch';
import { CustomPaginationPanel } from '../../../mainComponents/Tables/ExtendedTable/components//CustomPaginationPanel';
import { CustomSizePerPageButton } from '../../../mainComponents/Tables/ExtendedTable/components//CustomSizePerPageButton';
import { CustomPaginationTotal } from '../../../mainComponents/Tables/ExtendedTable/components//CustomPaginationTotal';
import { randomArray } from '../../../utilities';
import { NavLink } from 'react-router-dom';
import img1 from '../../../images/avatars/1.jpg';
const INITIAL_PRODUCTS_COUNT = 500;

const ProductQuality = {
    Good: 'product-quality__good',
    Bad: 'product-quality__bad',
    Unknown: 'product-quality__unknown'
};

const sortCaret = (order) => {
    if (!order)
        return <i className="fa fa-fw fa-sort text-muted"></i>;
    if (order)
        return <i className={`fa fa-fw text-muted fa-sort-${order}`}></i>
}

const generateRow = (index) => ({
    id: index,
    name: faker.commerce.productName(),
    quality: randomArray([
        ProductQuality.Bad,
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    price: (1000 + Math.random() * 1000).toFixed(2),
    satisfaction: Math.round(Math.random() * 6),
    inStockDate: faker.date.past()
});

export default class Index extends React.Component {
    constructor() {
        super();
        
        this.state = {
            products: _.times(INITIAL_PRODUCTS_COUNT, generateRow),
            selected: [],
            tableName:'Example Table',
            createFormRoute:"/admin/example-create"
        };

        this.headerCheckboxRef = React.createRef();
    }

    handleSelect(row, isSelected) {
        if (isSelected) {
            this.setState({ selected: [...this.state.selected, row.id] })
        } else {
            this.setState({
                selected: this.state.selected.filter(itemId => itemId !== row.id)
            })
        }
    }

    handleSelectAll(isSelected, rows) {
        if (isSelected) {
            this.setState({ selected: _.map(rows, 'id') })
        } else {
            this.setState({ selected: [] });
        }
    }

    handleAddRow() {
        const currentSize = this.state.products.length;
        console.log(this.state.products)
    }

    handleDeleteRow() {
        console.log(this.state.products)
       
    }

    handleResetFilters() {
        this.nameFilter('');
        this.qualityFilter('');
        this.priceFilter('');
        this.satisfactionFilter('');
    }

    createColumnDefinitions() {
        return [{
            dataField: 'id',
            text: 'Product ID',
        }, {
            dataField: 'name',
            text: 'Product Name',
         
        }, {
            dataField: 'quality',
            text: 'Product Quality',
            formatter: (cell) => {
                let pqProps;
                switch (cell) {
                    case ProductQuality.Good:
                        pqProps = {
                            color: 'success',
                            text: 'Good'
                        }
                    break;
                    case ProductQuality.Bad:
                        pqProps = {
                            color: 'danger',
                            text: 'Bad'
                        }
                    break;
                    case ProductQuality.Unknown:
                    default:
                        pqProps = {
                            color: 'secondary',
                            text: 'Unknown'
                        }
                }

                return (
                    <Badge color={pqProps.color}>
                        { pqProps.text }
                    </Badge>
                )
            }
        }, {
            dataField: 'price',
            text: 'Product Price',
        
        }, {
            dataField: 'satisfaction',
            text: 'Buyer Satisfaction',
          
        }, {
            dataField: 'inStockDate',
            text: 'In Stock From',
            formatter: (cell) =>
                moment(cell).format('DD/MM/YYYY'),
        },
        {
            dataField: 'image',
            text: 'Image',
            formatter: (cell) =>{
                return(
                    <img src={img1} height={90} width={90}/>
                )
            },
        
        },
        {
            dataField: 'Action',
            text: 'Action',
            formatter: (cell) =>{
                return(
                    <InputGroup>
                <InputGroupAddon addonType="prepend">
                <Button  color="secondary" outline>
                    Update 
                </Button>
                </InputGroupAddon>
                <InputGroupAddon addonType="prepend">
                <Button color="danger" outline>
                    Remove 
                </Button>
                </InputGroupAddon>
                </InputGroup>
                )
            },
        
        },
    ]; 
    }

    render() {
        const columnDefs = this.createColumnDefinitions();
        const paginationDef = paginationFactory({
            paginationSize: 5,
            showTotal: true,
            pageListRenderer: (props) => (
                <CustomPaginationPanel { ...props } size="sm" className="ml-md-auto mt-2 mt-md-0" />
            ),
            sizePerPageRenderer: (props) => (
                <CustomSizePerPageButton { ...props } />
            ),
            paginationTotalRenderer: (from, to, size) => (
                <CustomPaginationTotal { ...{ from, to, size } } />
            )
        });
        const selectRowConfig = {
            mode: 'checkbox',
            selected: this.state.selected,
            onSelect: this.handleSelect.bind(this),
            onSelectAll: this.handleSelectAll.bind(this),
            selectionRenderer: ({ mode, checked, disabled }) => (
                <CustomInput type={ mode } checked={ checked } disabled={ disabled } />
            ),
            selectionHeaderRenderer: ({ mode, checked, indeterminate }) => (
                <CustomInput type={ mode } checked={ checked } innerRef={el => el && (el.indeterminate = indeterminate)} />
            )
        };

        return (
            <ToolkitProvider
                keyField="id"
                data={ this.state.products }
                columns={ columnDefs }
                search
                exportCSV
            >
            {
                props => (
                    <React.Fragment>
                        <div className="d-flex justify-content-end align-items-center mb-2">
                            <h6 className="my-0">
                                {this.state.tableName}
                            </h6>
                            <div className="d-flex ml-auto">
                                <CustomSearch
                                    className="mr-2"
                                    { ...props.searchProps }
                                />
                                <ButtonGroup>
                                    <CustomExportCSV
                                        { ...props.csvProps }
                                    >
                                        Export
                                    </CustomExportCSV>
                                    <NavLink to={`${this.state.createFormRoute}`}>
                                    <Button
                                        size="sm"
                                        outline
                                        
                                    >

                                        <i className="fa fa-fw fa-plus"></i>
                                    </Button>
                                        </NavLink>
                                </ButtonGroup>
                            </div>
                        </div>
                        <BootstrapTable
                            classes="table-responsive"
                            pagination={ paginationDef }
                            filter={ filterFactory() }
                            selectRow={ selectRowConfig }
                            bordered={ false }
                            responsive
                            { ...props.baseProps }
                        />
                    </React.Fragment>
                )
            }
            </ToolkitProvider>
        );
    }
}