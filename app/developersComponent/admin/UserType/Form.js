import React from 'react';

import { 
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    FormFeedback,
    Badge,
    CustomInput,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText,
    Button,
    InputGroup,
    InputGroupAddon
} from '../../../components';

import Image from '../../../components/ImageUploader/index'

import { HeaderMain } from "../../../mainComponents/components/HeaderMain";
import { HeaderDemo } from "../../../mainComponents/components/HeaderDemo";
import ImageUploading from 'react-images-uploading';
import axiosInstance from '../../../axios.js'
// import 'rsuite/dist/styles/rsuite-default.css';
import {postusertype} from '../../../store/usertype/action.js';
import { connect } from "react-redux";

import { MultiCascader } from 'rsuite';         
const data=[
  {
    "value": "1",
    "label": "四川",
    "children": [
          {
            "value": "1-1-1",
            "label": "锦江区"
          },
          {
            "value": "1-1-2",
            "label": "青羊区"
          },
          {
            "value": "1-1-3",
            "label": "金牛区"
          },
          {
            "value": "1-1-4",
            "label": "武侯区"
          },
          {
            "value": "1-1-5",
            "label": "成华区"
          }
        ]
     
  }
]
class ExampleForm extends React.Component{
    

    state={
        formTitle:'Create User Type',
        images:[],
        form:{},
        maxNumberOfImage:5,
        user_type:'',
        data:[],
        value:[],
        modelPermissions:[],
    }


    componentDidMount(){
        axiosInstance.post('api/v1/getData/',{'queryFor':'Permissions'}).then(res=>{
            console.log(res)
            if(res.data){
                this.state.data=res.data['data']
                this.state.modelPermissions=res.data['modelPermissions']
                this.setState({data:this.state.data,modelPermissions:this.state.modelPermissions})
                console.log(this.state.data,this.state.modelPermissions)
            }
        })
        
    }

     

    render(){
        const submit = (e) => {
            // data for submit
            e.preventDefault();
          
            
             let data={
                'permissions':this.state.value,
                'user_type':this.state.user_type,
            }
            this.props.postusertype(data,(res)=>{
                console.log(res,'team')
                if (res.status==201){

                console.log('user type Created')
                alert('user type Created')
                this.props.history.push("/admin/user-type");

                }

            },
            (err)=>{
                alert('Something Went Wronng')
            })
            
          };

    

  const handleChange=(value)=> {
      this.state.value=value
    this.setState({ value:this.state.value });
    console.log(this.state.value)
  }
        return(
            <React.Fragment>
            <Container>
           
                    <Card className="mb-3">
                        <CardBody>
                            <CardTitle tag="h6" className="mb-4">
                                {this.state.formTitle}
                               
                            </CardTitle>
                            <Form>
                                <Row>
                                    <Col lg={6}>
                                    <Row>
                                        <Col lg={6}>
                                            <FormGroup>
                                        <Label for="input-2">
                                            User Type
                                        </Label>
                                        <Input 
                                            type="text" 
                                            name="user_type" 
                                            id="input-1" 
                                            onChange={(e)=>this.setState({user_type:e.target.value})}
                                            placeholder="Enter The User Type..." 
                                        />
                                        </FormGroup>
                                        </Col>
                                      
                                    </Row>
                                    </Col>
                                   
                                </Row>
                                <Row>
                                 <MultiCascader
                                inline
                                data={this.state.data}
                                searchable={false}
                                menuHeight="auto"
                                menuWidth={180}
                                onChange={handleChange}
                            />
                                </Row>
                                
                                <Row>
                                    <Col lg={6}>
                                        <Button onClick={(e)=>submit(e)} color="primary">Save</Button>   
                                    </Col>
                                </Row>
                                </Form>
                             
            
                            { /* END Form */}
                        </CardBody>
                    </Card>
              
            { /* END Section 1 */}
           
        </Container>
        
        </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
      state:state,
      token: state.auth.token,
      loading: state.auth.loading,
      error: state.auth.error,
      successAuth: state.auth.successAuth
    };
  };
const mapDispatchToProps = dispatch => {
    return {
        postusertype:(usertype, onSuccess , onError )=>dispatch(postusertype(usertype, onSuccess , onError ))
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(ExampleForm);
