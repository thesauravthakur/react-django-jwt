import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { Comparator, dateFilter } from 'react-bootstrap-table2-filter'
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import _ from 'lodash';
import faker from 'faker/locale/en_US';
import moment from 'moment';
import {
    Badge,
    Button,
    CustomInput,
    StarRating,
    ButtonGroup,
    InputGroup,
    InputGroupAddon
} from '../../../components';
import { CustomExportCSV } from '../../../mainComponents/Tables/ExtendedTable/components/CustomExportButton';
import { CustomSearch } from '../../../mainComponents/Tables/ExtendedTable/components/CustomSearch';
import { CustomPaginationPanel } from '../../../mainComponents/Tables/ExtendedTable/components/CustomPaginationPanel';
import { CustomSizePerPageButton } from '../../../mainComponents/Tables/ExtendedTable/components/CustomSizePerPageButton';
import { CustomPaginationTotal } from '../../../mainComponents/Tables/ExtendedTable/components/CustomPaginationTotal';
import { randomArray } from '../../../utilities';
import { NavLink } from 'react-router-dom';
import img1 from '../../../images/avatars/1.jpg';
import {
    buildCustomTextFilter,
    buildCustomSelectFilter,
    buildCustomNumberFilter
} from './../../../mainComponents/Tables/ExtendedTable/filters';
import { connect } from "react-redux";
import {getusertype} from '../../../store/usertype/action.js';

const INITIAL_data_COUNT = 500;

const ProductQuality = {
    Good: 'product-quality__good',
    Bad: 'product-quality__bad',
    Unknown: 'product-quality__unknown'
};

const sortCaret = (order) => {
    if (!order)
        return <i className="fa fa-fw fa-sort text-muted"></i>;
    if (order)
        return <i className={`fa fa-fw text-muted fa-sort-${order}`}></i>
}

const generateRow = (index) => ({
    id: index,
    name: faker.commerce.productName(),
    quality: randomArray([
        ProductQuality.Bad,
        ProductQuality.Good,
        ProductQuality.Unknown
    ]),
    price: (1000 + Math.random() * 1000).toFixed(2),
    satisfaction: Math.round(Math.random() * 6),
    inStockDate: faker.date.past()
});

 class Index extends React.Component {
    constructor() {
        super();
        
        this.state = {
            // data: _.times(INITIAL_data_COUNT, generateRow),
            selected: [],
            data:[]
        };

        this.headerCheckboxRef = React.createRef();
    }

    componentDidMount(){
        this.props.getusertype((res)=>{
            console.log(res)
            if(res.status==200) {
                this.state.data=res.data
                this.setState({data:this.state.data})
            }
        },
        (err)=>{
            console.log(err)
        }
        )
    }

    handleSelect(row, isSelected) {
        if (isSelected) {
            this.setState({ selected: [...this.state.selected, row.id] })
        } else {
            this.setState({
                selected: this.state.selected.filter(itemId => itemId !== row.id)
            })
        }
    }

    handleSelectAll(isSelected, rows) {
        if (isSelected) {
            this.setState({ selected: _.map(rows, 'id') })
        } else {
            this.setState({ selected: [] });
        }
    }

    handleAddRow() {
        const currentSize = this.state.data.length;

        this.setState({
            data: [
                generateRow(currentSize + 1),
                ...this.state.data,
            ]
        });
    }

    handleDeleteRow() {
        this.setState({
            data: _.filter(this.state.data, product =>
                !_.includes(this.state.selected, product.id))
        })
    }

    handleResetFilters() {
        this.nameFilter('');
        this.qualityFilter('');
        this.priceFilter('');
        this.satisfactionFilter('');
    }

    createColumnDefinitions() {
        return [{
            dataField: 'id',
            text: 'Product ID',
            headerFormatter: column => (
                <React.Fragment>
                    <span className="text-nowrap">{ column.text }</span>
                    <a
                        href="javascript:;"
                        className="d-block small text-decoration-none text-nowrap"
                        onClick={ this.handleResetFilters.bind(this) }
                    >
                        Reset Filters <i className="fa fa-times fa-fw text-danger"></i>
                    </a>
                </React.Fragment>
            )
        }, {
            dataField: 'user_type',
            text: 'Product Name',
            sort: true,
            sortCaret,
            formatter: (cell) => (
                <span className="text-inverse">
                    { cell }
                </span>
            ),
            ...buildCustomTextFilter({
                placeholder: 'Enter product name...',
                getFilter: filter => { this.nameFilter = filter; }
            })
        }, 
        {
            dataField: 'Action',
            text: 'Action',
            formatter: (cell) =>{
                return(
                    <InputGroup>
                <InputGroupAddon addonType="prepend">
                <Button  color="secondary" outline>
                    Update 
                </Button>
                </InputGroupAddon>
                <InputGroupAddon addonType="prepend">
                <Button color="danger" outline>
                    Remove 
                </Button>
                </InputGroupAddon>
                </InputGroup>
                )
            },
        
        },]; 
    }

    render() {
        const columnDefs = this.createColumnDefinitions();
        const paginationDef = paginationFactory({
            paginationSize: 5,
            showTotal: true,
            pageListRenderer: (props) => (
                <CustomPaginationPanel { ...props } size="sm" className="ml-md-auto mt-2 mt-md-0" />
            ),
            sizePerPageRenderer: (props) => (
                <CustomSizePerPageButton { ...props } />
            ),
            paginationTotalRenderer: (from, to, size) => (
                <CustomPaginationTotal { ...{ from, to, size } } />
            )
        });
        const selectRowConfig = {
            mode: 'checkbox',
            selected: this.state.selected,
            onSelect: this.handleSelect.bind(this),
            onSelectAll: this.handleSelectAll.bind(this),
            selectionRenderer: ({ mode, checked, disabled }) => (
                <CustomInput type={ mode } checked={ checked } disabled={ disabled } />
            ),
            selectionHeaderRenderer: ({ mode, checked, indeterminate }) => (
                <CustomInput type={ mode } checked={ checked } innerRef={el => el && (el.indeterminate = indeterminate)} />
            )
        };

        return (
            <ToolkitProvider
                keyField="id"
                data={ this.state.data }
                columns={ columnDefs }
                search
                exportCSV
            >
            {
                props => (
                    <React.Fragment>
                        <div className="d-flex justify-content-end align-items-center mb-2">
                            <h6 className="my-0">
                                AdvancedTable A
                            </h6>
                            <div className="d-flex ml-auto">
                                <CustomSearch
                                    className="mr-2"
                                    { ...props.searchProps }
                                />
                                <ButtonGroup>
                                    <CustomExportCSV
                                        { ...props.csvProps }
                                    >
                                        Export
                                    </CustomExportCSV>
                                    <Button
                                        size="sm"
                                        outline
                                        onClick={ this.handleDeleteRow.bind(this) }
                                    >
                                        Delete
                                    </Button>
                                    <NavLink
                                        to='/admin/user-type-create'
                                    >

                                    <Button
                                        size="sm"
                                        outline
                                        // onClick={ this.handleAddRow.bind(this) }
                                        >
                                        <i className="fa fa-fw fa-plus"></i>
                                    </Button>
                                        </NavLink>
                                </ButtonGroup>
                            </div>
                        </div>
                        <BootstrapTable
                            classes="table-responsive"
                            style={{width:'100%'}}
                            pagination={ paginationDef }
                            filter={ filterFactory() }
                            // selectRow={ selectRowConfig }
                            bordered={ false }
                            responsive
                            { ...props.baseProps }
                        />
                    </React.Fragment>
                )
            }
            </ToolkitProvider>
        );
    }
}

const mapStateToProps = state => {
    return {
      state:state,
      token: state.auth.token,
      loading: state.auth.loading,
      error: state.auth.error,
      successAuth: state.auth.successAuth
    };
  };
const mapDispatchToProps = dispatch => {
    return {
        getusertype:( onSuccess , onError )=>dispatch(getusertype( onSuccess , onError ))
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(Index);


