import React from 'react';

import { 
    Container,
    Row,
    Col,
    Card,
    CardTitle,
    CardBody,
    FormFeedback,
    Badge,
    CustomInput,
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText,
    Button,
    InputGroup,
    InputGroupAddon
} from '../../../components';

import Image from '../../../components/ImageUploader/index'

import { HeaderMain } from "../../../mainComponents/components/HeaderMain";
import { HeaderDemo } from "../../../mainComponents/components/HeaderDemo";
import ImageUploading from 'react-images-uploading';
export default class ExampleForm extends React.Component{

    state={
        formTitle:'Create User',
        images:[],
        maxNumberOfImage:5
    }

    render(){
        const submit = () => {
            // data for submit
            console.log(this.state.images)
          };

        return(
            <React.Fragment>
            <Container>
           
                    <Card className="mb-3">
                        <CardBody>
                            <CardTitle tag="h6" className="mb-4">
                                {this.state.formTitle}
                               
                            </CardTitle>
                                <Row>
                                    <Col lg={6}>
                                    <Row>
                                        <Col lg={6}>
                                            <FormGroup>
                                        <Label for="input-2">
                                            First Name
                                        </Label>
                                        <Input 
                                            type="text" 
                                            name="first_name" 
                                            id="input-1" 
                                            placeholder="Enter First Name..." 
                                        />
                                        </FormGroup>
                                        </Col>
                                        <Col lg={6}>
                                            <FormGroup>
                                        <Label for="input-2">
                                            Last Name
                                        </Label>
                                        <Input 
                                            type="text" 
                                            name="last_name" 
                                            id="input-2" 
                                            placeholder="Enter Last Name..." 
                                        />
                                        </FormGroup>
                                        </Col>
                                    </Row>
                                    </Col>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="inputPassword-2">
                                    Email
                                    </Label>
                                    <Input 
                                        type="email" 
                                        name="email" 
                                        id="test-3" 
                                        placeholder="Enter Your Email..." 
                                    />
                                </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="operatingSystem" className="pt-0">
                                    Gender
                                    </Label>
                                    <div>
                                        <CustomInput 
                                            type="radio" 
                                            id="1"
                                            name="gender"
                                            label="Male" 
                                            inline
                                            defaultChecked
                                        />
                                        <CustomInput 
                                            type="radio" 
                                            id="2" 
                                            name="gender"
                                            label="Female" 
                                            inline
                                        /> 
                                  
                                      
                                        
                                    </div>
                                </FormGroup>
                                    </Col>
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="input-4">
                                    Address
                                    </Label>
                                    <Input 
                                        type="text" 
                                        name="" 
                                        id="input-4" 
                                        placeholder="Enter Your Address..." 
                                    />
                                    </FormGroup>
                                    </Col>
                                </Row>
                          
                               <Row>
                                   <Col lg={6}>
                                   <FormGroup>
                                    <Label for="country-selector-2">
                                    User Type
                                    </Label>
                                    <CustomInput 
                                        type="select" 
                                        name="user_type" 
                                        id="country-selector-2"  
                                    >
                                        <option value="">Select Country...</option>
                                        <option>United States of America (US)</option>
                                        <option>United Kingdom (UK)</option>
                                        <option>Australia</option>
                                        <option>Canada</option>
                                        <option>Other...</option>
                                    </CustomInput>
                                </FormGroup>
                                   </Col>
                                   <Col lg={6}>
                                    <FormGroup>
                                    <Label for="addCv2">
                                        File 
                                    </Label>
                                    <CustomInput type="file" id="addCv2" name="customFile" label="Choose file..." />
                                    <FormText color="muted">
                                        Accepted formats: pdf, doc, txt. 
                                    </FormText>
                                </FormGroup>
                                    </Col>
                               </Row>
                               
                                <Row>
                                <Col lg={6}>
                                <Image 
                                images={this.state.images} 
                                parentCallback = {(images)=>this.setState({images: images})}
                                maxNumberOfImage={this.state.maxNumberOfImage}
                                />
                                   
                                    </Col>  
                                    <Col lg={6}>
                                    <FormGroup>
                                    <Label for="message-2">
                                        Message
                                    </Label>
                                        <Input 
                                            type="textarea" 
                                            name="text" 
                                            id="message-2" 
                                            placeholder="Enter Your Message..." 
                                            className="mb-2"
                                        />
                                        
                                </FormGroup>
                                    </Col>
                                </Row>
                              
                                <Row>
                                    <Col lg={6}>
                                    <CustomInput 
                                            type="checkbox" 
                                            id="iConfirm" 
                                            label="I confirm that I have read the Terms." 
                                            className="mb-3"
                                        />
                                        <Button onClick={()=>submit()} color="primary">Save</Button>   
                                    </Col>
                                </Row>
                             
            
                            { /* END Form */}
                        </CardBody>
                    </Card>
              
            { /* END Section 1 */}
           
        </Container>
        
        </React.Fragment>
        )
    }
}