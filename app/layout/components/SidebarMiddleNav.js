import React from 'react';

import { SidebarMenu } from './../../components';
import {navItems} from '../sidebarData.js';
export const SidebarMiddleNav = () => (
    <SidebarMenu>
        {/* <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-home"></i>}
            title="Products"
        >
            <SidebarMenu.Item title="Products" to='/admin' exact />
        </SidebarMenu.Item> 
        <SidebarMenu.Item
            icon={<i className="fa fa-fw fa-home"></i>}
            title="Dashboards"
        >
            <SidebarMenu.Item title="Projects 1" to='/admin/dashboards/projects' exact />
        </SidebarMenu.Item>  */}
        {
            navItems.map((item,index)=>{
                return(
                    <SidebarMenu.Item
                    key={index}
                    icon={<i className={`${item.icon}`}></i>}
                    title={item.title}
                    to={item.route&&item.route}
                >
                    {item.child.length>0 &&
                    
                        item.child.map((item1, index1)=>{
                           return(
                            <SidebarMenu.Item key={index1} title={`${item1.title}`} to={`${item1.route}`} exact />
                           ) 
                        })

                    }
                    </SidebarMenu.Item>   
                )
            })
        }      
    </SidebarMenu >
);
