export const navItems=[
    {
        'title':'Dashboard',
        'icon':'fa fa-fw fa-home',
        'child':[
            {
                'title':'Project 1',
                'route':'/admin/dashboards/projects',
                
            },
            {
                'title':'Project 2',
                'route':'/admin/dashboards/projects'
            }

        ]
    },
    {
        'title':'User Configuration',
        'icon':'fa fa-fw fa-home',
        'child':[
            {
                'title':'User Types',
                'route':'/admin/user-type'
            },
            {
                'title':'Users',
                'route':'/admin/users'
            }

        ]
    }
    ,
    {
        'title':'System Edit',
        'icon':'fa fa-cog',
        'route':'/admin/profile-edit',
        'child':[]

    }
    
]