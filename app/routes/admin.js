import React from 'react';
import {
    Route,
    Switch,
    Redirect
} from 'react-router';

// ----------- Pages Imports ---------------
import ProjectsDashboard from '../mainComponents/Dashboards/Projects';
import { authLogin } from '../store/actions/auth.js';
import {useSelector,useDispatch} from 'react-redux'
import {adminRoute} from './adminRouteData'
//------ Route Definitions --------
// eslint-disable-next-line no-unused-vars

export const AdminRoutedContent = () => {
     const state=useSelector(state=>state)
     const dispatch=useDispatch()
     console.log('test',state)
     var auth=state.auth.successAuth
    return (

                <React.Fragment>
                    <Switch>
                    {/* <Route path="/admin/dashboards/projects"  component={ProjectsDashboard} /> */}
                    {
                        adminRoute.map((item,index)=>{
                            return(
                            <Route path={`${item.path}`} component={item.component} />
                            )
                        })
                    }
                    </Switch>
                </React.Fragment>
);
    }



