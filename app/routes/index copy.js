import React from 'react';
import {
    Route,
    Switch,
    Redirect
} from 'react-router';

// ----------- Pages Imports ---------------
import Analytics from '../mainComponents/Dashboards/Analytics';
import ProjectsDashboard from '../mainComponents/Dashboards/Projects';
import System from '../mainComponents/Dashboards/System';
import Monitor from '../mainComponents/Dashboards/Monitor'; 
import Financial from '../mainComponents/Dashboards/Financial';
import Stock from '../mainComponents/Dashboards/Stock';
import Reports from '../mainComponents/Dashboards/Reports';

import Widgets from '../mainComponents/Widgets';

import Cards from '../mainComponents/Cards/Cards';
import CardsHeaders from '../mainComponents/Cards/CardsHeaders';

import LayoutNavbar from '../mainComponents/Layouts/NavbarOnly';
import SidebarDefault from '../mainComponents/Layouts/SidebarDefault';
import SidebarA from '../mainComponents/Layouts/SidebarA';
import DragAndDropLayout from '../mainComponents/Layouts/DragAndDropLayout';
import SidebarWithNavbar from '../mainComponents/Layouts/SidebarWithNavbar';

import Accordions from '../mainComponents/Interface/Accordions';
import Alerts from '../mainComponents/Interface/Alerts';
import Avatars from '../mainComponents/Interface/Avatars';
import BadgesLabels from '../mainComponents/Interface/BadgesLabels';
import Breadcrumbs from '../mainComponents/Interface/Breadcrumbs';
import Buttons from '../mainComponents/Interface/Buttons';
import Colors from '../mainComponents/Interface/Colors';
import Dropdowns from '../mainComponents/Interface/Dropdowns';
import Images from '../mainComponents/Interface/Images';
import ListGroups from '../mainComponents/Interface/ListGroups';
import MediaObjects from '../mainComponents/Interface/MediaObjects';
import Modals from '../mainComponents/Interface/Modals';
import Navbars from '../mainComponents/Interface/Navbars';
import Paginations from '../mainComponents/Interface/Paginations';
import ProgressBars from '../mainComponents/Interface/ProgressBars';
import TabsPills from '../mainComponents/Interface/TabsPills';
import TooltipPopovers from '../mainComponents/Interface/TooltipsPopovers';
import Typography from '../mainComponents/Interface/Typography';
import Notifications from '../mainComponents/Interface/Notifications';
import CropImage from '../mainComponents/Interface/CropImage';
import DragAndDropElements from '../mainComponents/Interface/DragAndDropElements';
import Calendar from '../mainComponents/Interface/Calendar';
import ReCharts from '../mainComponents/Graphs/ReCharts';

import Forms from '../mainComponents/Forms/Forms';
import FormsLayouts from '../mainComponents/Forms/FormsLayouts';
import InputGroups from '../mainComponents/Forms/InputGroups';
import Wizard from '../mainComponents/Forms/Wizard';
import TextMask from '../mainComponents/Forms/TextMask';
import Typeahead from '../mainComponents/Forms/Typeahead';
import Toggles from '../mainComponents/Forms/Toggles';
import Editor from '../mainComponents/Forms/Editor';
import DatePicker from '../mainComponents/Forms/DatePicker';
import Dropzone from '../mainComponents/Forms/Dropzone';
import Sliders from '../mainComponents/Forms/Sliders';

import Tables from '../mainComponents/Tables/Tables';
import ExtendedTable from '../mainComponents/Tables/ExtendedTable';
import AgGrid from '../mainComponents/Tables/AgGrid';

import AccountEdit from '../mainComponents/Apps/AccountEdit';
import BillingEdit from '../mainComponents/Apps/BillingEdit';
import ProfileEdit from '../mainComponents/Apps/ProfileEdit';
import SessionsEdit from '../mainComponents/Apps/SessionsEdit';
import SettingsEdit from '../mainComponents/Apps/SettingsEdit';
import Chat from '../mainComponents/Apps/Chat';
import Clients from '../mainComponents/Apps/Clients';
import EmailDetails from '../mainComponents/Apps/EmailDetails';
import Files from '../mainComponents/Apps/Files';
import GalleryGrid from '../mainComponents/Apps/GalleryGrid';
import GalleryTable from '../mainComponents/Apps/GalleryTable';
import ImagesResults from '../mainComponents/Apps/ImagesResults';
import Inbox from '../mainComponents/Apps/Inbox';
import NewEmail from '../mainComponents/Apps/NewEmail';
import ProfileDetails from '../mainComponents/Apps/ProfileDetails';
import Projects from '../mainComponents/Apps/Projects';
import SearchResults from '../mainComponents/Apps/SearchResults';
import Tasks from '../mainComponents/Apps/Tasks';
import TasksDetails from '../mainComponents/Apps/TasksDetails';
import TasksKanban from '../mainComponents/Apps/TasksKanban';
import Users from '../mainComponents/Apps/Users';
import UsersResults from '../mainComponents/Apps/UsersResults';
import VideosResults from '../mainComponents/Apps/VideosResults';

import ComingSoon from '../mainComponents/Pages/ComingSoon';
import Confirmation from '../mainComponents/Pages/Confirmation';
import Danger from '../mainComponents/Pages/Danger';
import Error404 from '../mainComponents/Pages/Error404';
import ForgotPassword from '../mainComponents/Pages/ForgotPassword';
import LockScreen from '../mainComponents/Pages/LockScreen';
import Login from '../mainComponents/Pages/Login';
import Register from '../mainComponents/Pages/Register';
import Success from '../mainComponents/Pages/Success';
import Timeline from '../mainComponents/Pages/Timeline';

import Icons from '../mainComponents/Icons';

// ----------- Layout Imports ---------------
import { DefaultNavbar } from '../layout/components/DefaultNavbar';
import { DefaultSidebar } from '../layout/components/DefaultSidebar';

import { SidebarANavbar } from '../layout/components/SidebarANavbar';
import { SidebarASidebar } from '../layout/components/SidebarASidebar';
import { authLogin } from '../store/actions/auth.js';
import {useSelector,useDispatch} from 'react-redux'

//------ Route Definitions --------
// eslint-disable-next-line no-unused-vars
export const RoutedContent = () => {
     const state=useSelector(state=>state)
     const dispatch=useDispatch()
     console.log('test',state)
    return (
        <Switch>
            <Redirect from="/" to="/public/layouts/navbar" exact />
            
            <Route path="/admin/dashboards/analytics" exact component={Analytics} />
            <Route path="/admin/dashboards/projects" exact component={ProjectsDashboard} />
            <Route path="/admin/dashboards/system" exact component={System} />
            <Route path="/admin/dashboards/monitor" exact component={Monitor} />
            <Route path="/admin/dashboards/financial" exact component={Financial} />
            <Route path="/admin/dashboards/stock" exact component={Stock} />
            <Route path="/admin/dashboards/reports" exact component={Reports} />

            <Route path='/admin/widgets' exact component={Widgets} />
            
            { /*    Cards Routes     */ }
            <Route path='/admin/cards/cards' exact component={Cards} />
            <Route path='/admin/cards/cardsheaders' exact component={CardsHeaders} />
            
            { /*    Layouts     */ }
            <Route path='/public/layouts/navbar' component={LayoutNavbar} />
            <Route path='/admin/layouts/sidebar' component={SidebarDefault} />
            <Route path='/admin/layouts/sidebar-a' component={SidebarA} />
            <Route path="/admin/layouts/sidebar-with-navbar" component={SidebarWithNavbar} />
            <Route path='/admin/layouts/dnd-layout' component={DragAndDropLayout} />

            { /*    Interface Routes   */ }
            <Route component={ Accordions } path="/admin/interface/accordions" />
            <Route component={ Alerts } path="/admin/interface/alerts" />
            <Route component={ Avatars } path="/admin/interface/avatars" />
            <Route component={ BadgesLabels } path="/admin/interface/badges-and-labels" />
            <Route component={ Breadcrumbs } path="/admin/interface/breadcrumbs" />
            <Route component={ Buttons } path="/admin/interface/buttons" />
            <Route component={ Colors } path="/admin/interface/colors" />
            <Route component={ Dropdowns } path="/admin/interface/dropdowns" />
            <Route component={ Images } path="/admin/interface/images" />
            <Route component={ ListGroups } path="/admin/interface/list-groups" />
            <Route component={ MediaObjects } path="/admin/interface/media-objects" />
            <Route component={ Modals } path="/admin/interface/modals" />
            <Route component={ Navbars } path="/admin/interface/navbars" />
            <Route component={ Paginations } path="/admin/interface/paginations" />
            <Route component={ ProgressBars } path="/admin/interface/progress-bars" />
            <Route component={ TabsPills } path="/admin/interface/tabs-pills" />
            <Route component={ TooltipPopovers } path="/admin/interface/tooltips-and-popovers" />
            <Route component={ Typography } path="/admin/interface/typography" />
            <Route component={ Notifications } path="/admin/interface/notifications" />
            <Route component={ CropImage } path="/admin/interface/crop-image" />
            <Route component={ DragAndDropElements } path="/admin/interface/drag-and-drop-elements" />
            <Route component={ Calendar } path="/admin/interface/calendar" />

            { /*    Forms Routes    */ }
            <Route component={ Forms } path="/admin/forms/forms" />
            <Route component={ FormsLayouts } path="/admin/forms/forms-layouts" />
            <Route component={ InputGroups } path="/admin/forms/input-groups" />
            <Route component={ Wizard } path="/admin/forms/wizard" />
            <Route component={ TextMask } path="/admin/forms/text-mask" />
            <Route component={ Typeahead } path="/admin/forms/typeahead" />
            <Route component={ Toggles } path="/admin/forms/toggles" />
            <Route component={ Editor } path="/admin/forms/editor" />
            <Route component={ DatePicker } path="/admin/forms/date-picker" />
            <Route component={ Dropzone } path="/admin/forms/dropzone" />
            <Route component={ Sliders } path="/admin/forms/sliders" />
            
            { /*    Graphs Routes   */ }
            <Route component={ ReCharts } path="/admin/graphs/re-charts" />

            { /*    Tables Routes   */ }
            <Route component={ Tables } path="/admin/tables/tables" />
            <Route component={ ExtendedTable } path="/admin/tables/extended-table" />
            <Route component={ AgGrid } path="/admin/tables/ag-grid" />

            { /*    Apps Routes     */ }
            <Route component={ AccountEdit } path="/admin/apps/account-edit" />
            <Route component={ BillingEdit } path="/admin/apps/billing-edit" />
            <Route component={ Chat } path="/admin/apps/chat" />
            <Route component={ Clients } path="/admin/apps/clients" />
            <Route component={ EmailDetails } path="/admin/apps/email-details" />
            <Route component={ Files } path="/admin/apps/files/:type"/>
            <Route component={ GalleryGrid } path="/admin/apps/gallery-grid" />
            <Route component={ GalleryTable } path="/admin/apps/gallery-table" />
            <Route component={ ImagesResults } path="/admin/apps/images-results" />
            <Route component={ Inbox } path="/admin/apps/inbox" />
            <Route component={ NewEmail } path="/admin/apps/new-email" />
            <Route component={ ProfileDetails } path="/admin/apps/profile-details" />
            <Route component={ ProfileEdit } path="/admin/apps/profile-edit" />
            <Route component={ Projects } path="/admin/apps/projects/:type" />
            <Route component={ SearchResults } path="/admin/apps/search-results" />
            <Route component={ SessionsEdit } path="/admin/apps/sessions-edit" />
            <Route component={ SettingsEdit } path="/admin/apps/settings-edit" />
            <Route component={ Tasks } path="/admin/apps/tasks/:type" />
            <Route component={ TasksDetails } path="/admin/apps/task-details" />
            <Route component={ TasksKanban } path="/admin/apps/tasks-kanban" />
            <Route component={ Users } path="/admin/apps/users/:type" />
            <Route component={ UsersResults } path="/admin/apps/users-results" />
            <Route component={ VideosResults } path="/admin/apps/videos-results" />

            { /*    Pages Routes    */ }
            <Route component={ ComingSoon } path="/admin/pages/coming-soon" />
            <Route component={ Confirmation } path="/admin/pages/confirmation" />
            <Route component={ Danger } path="/admin/pages/danger" />
            <Route component={ Error404 } path="/admin/pages/error-404" />
            <Route component={ ForgotPassword } path="/admin/pages/forgot-password" />
            <Route component={ LockScreen } path="/admin/pages/lock-screen" />
            <Route component={ Login } path="/admin/pages/login" />
            <Route component={ Register } path="/admin/pages/register" />
            <Route component={ Success } path="/admin/pages/success" />
            <Route component={ Timeline } path="/admin/pages/timeline" />

            <Route path='/admin/icons' exact component={Icons} />

            { /*    404    */ }
            <Redirect to="/admin/pages/error-404" />
        </Switch>
    );
};

//------ Custom Layout Parts --------
export const RoutedNavbars  = () => (
    <Switch>
        { /* Other Navbars: */}
        <Route
            component={ SidebarANavbar }
            path="/admin/layouts/sidebar-a"
        />
        {/* <Route
            component={ NavbarOnly.Navbar }
            path="/public/layouts/navbar"
        /> */}
        <Route
            component={ SidebarWithNavbar.Navbar }
            path="/admin/layouts/sidebar-with-navbar"
        />
        { /* Default Navbar: */}
        <Route
            component={ DefaultNavbar }
        />
    </Switch>  
);

export const RoutedSidebars = () => (
    <Switch>
        { /* Other Sidebars: */}
        <Route
            component={ SidebarASidebar }
            path="/admin/layouts/sidebar-a"
        />
        <Route
            component={ SidebarWithNavbar.Sidebar }
            path="/admin/layouts/sidebar-with-navbar"
        />
        { /* Default Sidebar: */}
        <Route
            component={ DefaultSidebar }
        />
    </Switch>
);
