import React from 'react'
import BillingEdit from '../mainComponents/Apps/BillingEdit';
import ProfileDetails from '../mainComponents/Apps/ProfileDetails';
import ProfileEdit from '../mainComponents/Apps/ProfileEdit';
import SettingsEdit from '../mainComponents/Apps/SettingsEdit';
import Dashboard from '../developersComponent/public/dashboard/index.js'
import Error404 from '../mainComponents/Pages/Error404';
import Login from '../mainComponents/Pages/Login';

// export const adminRoute=[
//     {

// }
// ]

class Test extends React.Component{
    render(){
        return(
            <h1>
                Saurav
            </h1>
        )
    }
}

class TestSecond extends React.Component{
    render(){
        return(
            <h1>
                Thakur
            </h1>
        )
    }
}

class PublicDashboard extends React.Component{
    render(){
        return(
            <h1>
                Dashboard
            </h1>
        )
    }
}

export const publicRoute=[
    {
        'component':Dashboard,
        'path':'/public/dashboard',
    },
    {
        'component':Test,
        'path':'/public/first',
    },
    {
        'component':TestSecond,
        'path':'/public/second',
    },
    {
        'component':BillingEdit,
        'path':'/public/billing-edit',
    },
    {
        'component':ProfileDetails,
        'path':'/public/profile-details',
    },
    {
        'component':ProfileEdit,
        'path':'/public/profile-edit',
    },
    {
        'component':SettingsEdit,
        'path':'settings-edit',
    },
    {
        'component':Error404,
        'path':"/public/pages/error-404",
    },
    {
        'component':Login,
        'path':"/public/pages/login",
    },

]