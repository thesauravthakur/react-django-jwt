import React from 'react';
import {
    Route,
    Switch,
    Redirect
} from 'react-router';

// ----------- Pages Imports ---------------
import ProjectsDashboard from '../mainComponents/Dashboards/Projects';


import NavbarOnly from '../mainComponents/Layouts/NavbarOnly';
import SidebarDefault from '../mainComponents/Layouts/SidebarDefault';
import SidebarWithNavbar from '../mainComponents/Layouts/SidebarWithNavbar';


import Login from '../mainComponents/Pages/Login';

// ----------- Layout Imports ---------------
import { DefaultNavbar } from '../layout/components/DefaultNavbar';
import { DefaultSidebar } from '../layout/components/DefaultSidebar';

import { SidebarANavbar } from '../layout/components/SidebarANavbar';
import { SidebarASidebar } from '../layout/components/SidebarASidebar';
import { authLogin } from '../store/actions/auth.js';
import Error404 from '../mainComponents/Pages/Error404';

import {useSelector,useDispatch} from 'react-redux'
// import {MainPublicClass} from '../newComponents/public.js';
import {PublicRoutedContent} from './public'
import {AdminRoutedContent} from './admin'
//------ Route Definitions --------
// eslint-disable-next-line no-unused-vars
export const RoutedContent = () => {
     const state=useSelector(state=>state)
     const dispatch=useDispatch()
     console.log('saurav thakurji',state)
     var auth=state.auth.successAuth
    return (
        <React.Fragment>
            {
            !auth?
              (  
              <Switch>
                    <Redirect from="/" to="/public/dashboard" exact />
                    <Redirect from="/admin" to="/public/pages/login" exact />
                    <Redirect from="/admin/dashboard" to="/public/pages/login" exact />
                    <Redirect from="/login" to="/public/pages/login" exact />
            
    
                { /*    Layouts     */ }
                <Route path='/public' component={PublicRoutedContent} />
                { /*    404    */ }
                <Redirect  to="/public/pages/error-404" />
                    </Switch>
              )
                :
               ( 
               <Switch>
                <Redirect from="/" to="/public/dashboard/" exact />
                <Redirect from="/admin" to="/admin/dashboards/projects" exact />
                <Redirect from="/login" to="/admin/dashboards/projects" exact />
                <Redirect from="/admin/pages/login" to="/admin/dashboards/projects" exact />
                {/* <Route path="/admin/dashboards/projects"  component={ProjectsDashboard} /> */}
                <Route path="/admin" component={AdminRoutedContent} />

                    { /*    Layouts     */ }
                    {/* <Route component={ Error404 } path="/admin/pages/error-404" /> */}
                    <Route path='/public' component={PublicRoutedContent} />
                    <Route path='/admin/layouts/sidebar' component={SidebarDefault} />

                    { /*    404    */ }
                    <Redirect  to="/admin/pages/error-404" />

            </Switch>
            )
                
            }
        </React.Fragment>
    );
};

//------ Custom Layout Parts --------
export const RoutedNavbars  = () => (
    <Switch>
        { /* Other Navbars: */}
        <Route
            component={ SidebarANavbar }
            path="/admin/layouts/sidebar-a"
        />
        <Route
            component={ NavbarOnly.Navbar }
            path="/public"
        />
        <Route
            component={ SidebarWithNavbar.Navbar }
            path="/admin/layouts/sidebar-with-navbar"
        />
        { /* Default Navbar: */}
        <Route
            component={ DefaultNavbar }
        />
    </Switch>  
);

export const RoutedSidebars = () => (
    <Switch>
        { /* Other Sidebars: */}
        <Route
            component={ SidebarASidebar }
            path="/admin/layouts/sidebar-a"
        />
        <Route
            component={ SidebarWithNavbar.Sidebar }
            path="/admin/layouts/sidebar-with-navbar"
        />
        { /* Default Sidebar: */}
        <Route
            component={ DefaultSidebar }
        />
    </Switch>
);
