import React from 'react'
import ProfileDetails from '../mainComponents/Apps/ProfileDetails';
import SettingsEdit from '../mainComponents/Apps/SettingsEdit';
import ProjectsDashboard from '../mainComponents/Dashboards/Projects';
import ExampleIndex from '../developersComponent/admin/Example/index';
import ExampleForm from '../developersComponent/admin/Example/Form';
import Error404 from '../mainComponents/Pages/Error404';
import SettingIndex from '../developersComponent/admin/Setting/index.js'
import ProfileEdit from '../mainComponents/Apps/ProfileEdit';
import BillingEdit from '../mainComponents/Apps/BillingEdit';
import AccountEdit from '../mainComponents/Apps/AccountEdit';
import NewEmail from '../mainComponents/Apps/NewEmail';
import EmailDetails from '../mainComponents/Apps/EmailDetails';
import UserIndex from '../developersComponent/admin/Users/index.js'
import UserForm from '../developersComponent/admin/Users/Form.js'
import UserTypeIndex from '../developersComponent/admin/UserType/index.js'
import UserTypeForm from '../developersComponent/admin/UserType/Form.js'




class PublicDashboard extends React.Component{
    render(){
        return(
            <h1>
                Dashboard
            </h1>
        )
    }
}


export const adminRoute=[
    {
        'component':PublicDashboard,
        'path':'/admin/dashboard',
    },
    {
        'component':ProjectsDashboard,
        'path':'/admin/dashboards/projects',
    },
    {
        'component':Error404,
        'path':"/admin/pages/error-404",
    },
    {
        'component':SettingIndex,
        'path':'/admin/settings-edit',
    },
    {
        'component':AccountEdit,
        'path':"/admin/account-edit",
    },
    {
        'component':ProfileEdit,
        'path':"/admin/profile-edit",
    },
    {
        'component':ProfileDetails,
        'path':"/admin/profile-details",
    },
    {
        'component':NewEmail,
        'path':"/admin/new-email",
    },
    {
        'component':EmailDetails,
        'path':"/admin/email-details",
    },
    {
        'component':UserIndex,
        'path':"/admin/users",
    },
    {
        'component':UserForm,
        'path':"/admin/user-create",
    },
    {
        'component':UserTypeIndex,
        'path':"/admin/user-type",
    },
    {
        'component':UserTypeForm,
        'path':"/admin/user-type-create",
    },

]