import React from 'react';
import {
    Route,
    Switch,
    Redirect
} from 'react-router';

// ----------- Pages Imports ---------------
import { authLogin } from '../store/actions/auth.js';
import {useSelector,useDispatch} from 'react-redux'
import {MainPublicClass} from '../publicSidebarSupressor/index';
import {publicRoute} from './publicRouteData'
//------ Route Definitions --------
// eslint-disable-next-line no-unused-vars


class Test extends React.Component{
    render(){
        return(
            <h1>
                Saurav
            </h1>
        )
    }
}

class Testsecond extends React.Component{
    render(){
        return(
            <h1>
                Thakur
            </h1>
        )
    }
}

class PublicDashboard extends React.Component{
    render(){
        return(
            <h1>
                Dashboard
            </h1>
        )
    }
}

export const PublicRoutedContent = () => {
     const state=useSelector(state=>state)
     const dispatch=useDispatch()
     console.log('test',state)
     var auth=state.auth.successAuth
    return (

                <MainPublicClass>
                <Switch>
                    {/* <Route path='/public/dashboard' component={PublicDashboard} />
                    <Route path='/public/first' component={Test} />
                    <Route path='/public/second' component={Testsecond} />
                    <Route  path='/public/profile-details' component={ ProfileDetails }/>
                    <Route component={ SettingsEdit } path="/public/settings-edit" />
                    <Route component={ BillingEdit } path="/public/billing-edit" />

                    <Route component={ ProfileEdit } path='/public/profile-edit' /> */}
                    {
                        publicRoute.map((item,index)=>{
                            return(
                            <Route path={`${item.path}`} component={item.component} />
                            )
                        })
                    }
                </Switch>
                </MainPublicClass>
);
    }



