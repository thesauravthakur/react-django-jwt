from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import RegisterUserSerializer,UserSerializer,UserTypeSerializer
from .models import User,UserType,UserWithPermission
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import AllowAny
from rest_framework import viewsets



from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from django.db import transaction
from django.contrib.auth.models import Permission


class UserViewSets(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class CustomUserCreate(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format='json'):
        print(request.data)
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BlacklistTokenUpdateView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = ()

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)
            


modelForPermission=['user','setting','usertype']


class UserTypeViewSets(viewsets.ModelViewSet):
    serializer_class = UserTypeSerializer
    queryset = UserType.objects.all()


    @transaction.atomic
    def create(self, request, *args, **kwargs):
        print('saurav thakur')
        print(request.data)
        data=request.data
        permissionData=[]
        permission_value_arry=data['permissions']
        for item in modelForPermission:
            temp={}
            temp['value']=item
            temp['label']=item.capitalize()
            temp['children']=[]
            for perms in Permission.objects.all():
                temp2={}
                if item in perms.codename.split('_'):
                    temp2['value']=perms.codename
                    temp2['label']=perms.name
                    temp['children'].append(temp2)
                    # print(perms.codename,perms.name,'found')
            permissionData.append(temp)
        # print(permission_value_arry)
        
        for model in modelForPermission:
            for permission in permission_value_arry:
                if model==permission:
                    for item in permissionData:
                        if item['value']==model:
                            for temp in item['children']:
                                permission_value_arry.append(temp['value'])
        # for item in modelForPermission:
        #     print(temp)
        print(modelForPermission)
        for item in modelForPermission:
            print(item)
            if item in permission_value_arry:
                print('found')
                permission_value_arry.remove(item)
        


        print(permission_value_arry)

        userTypeModel=UserType()
        userTypeModel.user_type=data['user_type']
        userTypeModel.save()

        for item in permission_value_arry:
            UserWithPermissionModel=UserWithPermission()
            if UserWithPermission.objects.filter(codename=item).exists():
                UserWithPermissionModel=UserWithPermission.objects.filter(codename=item)[0]
                print('user type exists')
                
            UserWithPermissionModel.name=f'Can {item.split("_")[0]}  {item.split("_")[1]}'
            UserWithPermissionModel.codename=item
            UserWithPermissionModel.save()
            userTypeModel.permissions.add(UserWithPermissionModel)
            userTypeModel.save()
        print('created')
        
        return Response( status=status.HTTP_201_CREATED)

