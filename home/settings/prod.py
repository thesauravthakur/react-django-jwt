'''Use this for production'''

from .base import *

DEBUG = True

ALLOWED_HOSTS += [
    '192.168.11.112', '127.0.0.1', 'localhost', 'soptest.stsolutions.com.np',
    'pkk.stsolutions.com.np','127.0.0.1:3000', 
]

CORS_ORIGIN_ALLOW_ALL = True

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

# '192.168.11.112',
#     '127.0.0.1',
#     'localhost',
#     'sop.stsolutions.com.np',

WSGI_APPLICATION = 'home.wsgi.prod.application'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'pkk',
#         'USER': 'hybrid',
#         'PASSWORD': 'pa$$word',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'rdj',
        'USER': 'hybrid',
        'PASSWORD': 'pa$$word',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# REST_FRAMEWORK = {
#     'DEFAULT_PERMISSION_CLASSES': (
#         'rest_framework.permissions.IsAuthenticatedOrReadOnly',
#         'rest_framework.permissions.IsAuthenticated',
#     ),
#     'DEFAULT_AUTHENTICATION_CLASSES': (
#                 'rest_framework_simplejwt.authentication.JWTAuthentication',

#     ),
# }

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator'
    },
]

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

CORS_ORIGIN_WHITELIST = ('http://localhost:8000', 'http://127.0.0.1:8000','http://localhost:3000','http://127.0.0.1:3000')
