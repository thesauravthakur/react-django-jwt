from django.shortcuts import render
from rest_framework import viewsets
from settings import models as SettingModels
from django.db import transaction
from settings import serializers as SettingSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from PIL import Image
from io import BytesIO
import shutil
import sys
import os
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from settings import serializers
from django.core.files.base import ContentFile
import base64
import io
from PIL import Image
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from settings.models import Setting
from users.models import User
from django.contrib.auth.models import Permission

modelForPermission=['user','setting','usertype']


@csrf_exempt
def get_data(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if data['queryFor']=='Permissions':
            permissionData=[]
            for item in modelForPermission:
                temp={}
                temp['value']=item
                temp['label']=item.capitalize()
                temp['children']=[]
                for perms in Permission.objects.all():
                    temp2={}
                    if item in perms.codename.split('_'):
                        temp2['value']=perms.codename
                        temp2['label']=perms.name
                        temp['children'].append(temp2)
                        print(perms.codename,perms.name,'found')
                permissionData.append(temp)
            print(permissionData)
            return JsonResponse({'data':permissionData,"modelPermissions":modelForPermission}, status=201,safe=False)
  
    # elif request.method == 'GET':
    #     # serializer = TransformerSerializer(transformer)
    #     print('saurav thakur')
    #     data = JSONParser().parse(request)
    #     print(data)
    #     return JsonResponse({'saurav':'saurav thakur'}, status=201)
    
    # elif request.method == 'PUT':
    #     data = JSONParser().parse(request)
    #     serializer = TransformerSerializer(transformer, data=data)
  
    #     if serializer.is_valid():
    #         serializer.save()
    #         return JsonResponse(serializer.data)
    #     return JsonResponse(serializer.errors, status=400)
  
    # elif request.method == 'DELETE':
    #     transformer.delete()
    #     return HttpResponse(status=204)