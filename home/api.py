from rest_framework.routers import DefaultRouter
from users.views import UserViewSets

from settings.views import (SettingViewSets)
from pkk.views import (SettingDataViewSets, SliderViewSets, PublicSliderList,PublicContactRequest)
from users.views import UserTypeViewSets

from storage.urls import storageUrlpatterns 


router = DefaultRouter()

router.register(r'settings', SettingViewSets)
router.register(r'Slider', SliderViewSets)
router.register(r'usertype',UserTypeViewSets)
router.register(r'PublicSlider', PublicSliderList)
router.register(r'PublicContactRequest', PublicContactRequest)
router.register(r'settingsData', SettingDataViewSets)

urlpatterns = router.urls
urlpatterns += storageUrlpatterns
