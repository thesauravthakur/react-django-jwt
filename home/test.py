from books.models import BookEntry

bookEntries = BookEntry.objects.order_by('id').filter(id__gt=2387)
prev = False
batch = 1146
def getZeros(integer, count):
    d = str(integer)
    while len(d) < count:
        d = '0' + d
    return d

for entry in bookEntries:
    currentBookId = entry.book_id
    if prev and prev == currentBookId:
        entry.batch = getZeros(batch,4)
        print(currentBookId,batch,getZeros(batch,4))
    else:
        entry.batch = getZeros(batch,4)
        print(currentBookId,batch,getZeros(batch,4))
        batch=batch+1
    entry.save()
    prev = entry.book_id
