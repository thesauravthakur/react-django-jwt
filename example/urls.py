from rest_framework.routers import DefaultRouter
from .views import TestViewSet

router = DefaultRouter()
router.register(r'test', TestViewSet)
exampleUrlpatterns = router.urls