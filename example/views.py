from rest_framework import viewsets
from .serializers import TestSerializers
from .models import Test
from rest_framework.response import Response


class TestViewSet(viewsets.ModelViewSet):

    queryset = Test.objects.all()
    serializer_class = TestSerializers
