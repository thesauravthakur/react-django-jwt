from rest_framework import viewsets
from .serializers import ImageSerializers
from . import models
from django.db import transaction
from rest_framework.response import Response
from django.core.files.base import ContentFile
import base64
import io
from PIL import Image
import os




def base64_file(data, name=None):
    _format, _img_str = data.split(';base64,')
    _name, ext = _format.split('/')
    if not name:
        name = _name.split(":")[-1]
    return ContentFile(base64.b64decode(_img_str),
                       name='{}.{}'.format(name, ext))


class ImageViewSet(viewsets.ModelViewSet):
	queryset = models.Image.objects.all()
	serializer_class = ImageSerializers


	@transaction.atomic
	def create(self, request, *args, **kwargs):
		# print(imgData)
		data=request.data
		print(data['type'])
		# print(data)
		# for item in models.Image.objects.all():
		# 	item.delete()

		if data['type']=='create':
			imgData = base64_file(data=data['data_url'], name=data['name'])
			model=models.Image()
			model.image=imgData
			model.save()
			return Response({'image':model.image.url,'id':model.id})
		elif data['type']=='update':
			image_path=data['image_url']
			image_path=image_path.split('/media/')[1]
			"""
			instance=models.Image.objects.get(id=int(data['image_id']))

			We Can use id as well but here we are using image link by analysing the future work

			"""
			instance=models.Image.objects.filter(image=image_path)[0]
			print(instance.image)
			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
			image_file_actual_path = BASE_DIR + data['image_url']
			if os.path.exists(image_file_actual_path):				
				imgData = base64_file(data=data['data_url'], name=data['name'])
				instance.image=imgData
				instance.save()
				os.remove(image_file_actual_path)
				print('updated')
				print(instance.image.url)
				return Response({'image':instance.image.url,'id':instance.id})
		elif data['type']=='delete':
			image_path=data['image_url']
			image_path=image_path.split('/media/')[1]
			"""
			instance=models.Image.objects.get(id=int(data['image_id']))

			We Can use id as well but here we are using image link by analysing the future work

			"""
			instance=models.Image.objects.filter(image=image_path)[0]
			print(instance.image)
			BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
			image_file_actual_path = BASE_DIR + data['image_url']
			if os.path.exists(image_file_actual_path):				
				instance.delete()
				os.remove(image_file_actual_path)
				print('deleted')
				print(instance.image.url)
				return Response({'message':'Image Deleted'})

		return Response({'image':'No image Found out Generated'})



