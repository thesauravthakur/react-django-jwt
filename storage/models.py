from django.db import models

# Create your models here.
def image_upload_path(instance, filename):
    return '/'.join(['imageStorage', filename])




class Image(models.Model):
	image = models.ImageField(blank=True,
                              null=True,
                              upload_to=image_upload_path)
	